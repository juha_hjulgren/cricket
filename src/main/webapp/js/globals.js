var user;
var ticket;
var gauges = {};

/**
Initializing function to get the app up and running.
**/
function initApp(){
	checkForUserGroup();
	createUnitFront();
	primeUnitSideButtons();	
	
	$("div.overview").on("click", "i.sys-button", function(){
		if(checkForAdmin()){
			if(!$(this).hasClass("read-only"))
				toggleUnitSwitch($(this));
		}
	});
	
	$("div.overview").on("change","select.resolution-select", function(){
		//here we will update the chart.
		console.log("change resolution");
	});
	
	$("div.overview").on("change", "input.advanced-value", function(){
		//get closest unit
		var unit = $(this).closest("div").find("[name='unit_code']").val();	
		//get this attribute
		var attrib = $(this).attr("name");
		//get the value
		var value = $(this).val();
		console.log("saving:" +unit+", "+attrib+", "+value);
		//save stuff
		saveSetting(unit, attrib, value);
	});	
}
/**
This function adds functionality to the small buttons on the left upper corner of unit panel.
**/
function primeUnitSideButtons(){
	$("div.overview").on("click", "i.side-button", function(){
		var fx = $(this).attr('name');
		if($(this).hasClass("pressed")){
			$(this).removeClass("pressed");
			$("#"+fx).hide();
		}
		else{
			$(this).addClass("pressed");
			$("#"+fx).show();
		}
	});
}

/** 
Store the ticket to use when talking with the server.
**/
function storeTicket(rawData){
	ticket = rawData.auth_ticket;
	sessionStorage.setItem("auth_ticket", ticket);
	delete rawData.auth_ticket;
}

/**
Find a unit attribute by code from an array of attributes. 
Convenience function.
**/
function findAttribute(attrib_array, code) {
    for (var i = 0, len = attrib_array.length; i < len; i++) {
        if (attrib_array[i].code === code)
            return attrib_array[i]; // Return as soon as the object is found
    }
    return null; // The object was not found
}

function showError(errorMsg){
	
}