/**	Overview contains frontpage codes.

**/
/*****here be the tricky part, front page. rest not much different once we get these right.****/
function createUnitFront(){
	if(!user || user.id < 0 )return;
	//get each unit from server
	getUnits();
}

/****************************unit data fetch*****************/
function getUnits(){
	//getUnitList as json.
	$.get("rest/unit/list"+"?auth_ticket="+ticket).done(function(data){
		if(data !== 'fail'){
			var raw = JSON.parse(JSON.stringify(data));
			//take ticket and remove from data.
			storeTicket(raw);
			
			var units = raw.unit;
			for(var i = 0; i < units.length;++i){
				//make each unit alive
				var internal_unit = units[i];
				//construct the whole deal
				constructUnit(internal_unit);
				//add event rows to table, one by one
				var tbl = $("#"+internal_unit.code+"_event").find(".history-body-main");
				var tblTempl = $("#history-table-row-model").html();
				for(var j = 0; j < internal_unit.events.length;++j){
					var evt = internal_unit.events[j];
					tbl.append(Mustache.render(tblTempl, evt));
				}
				
				createChart(internal_unit);
				//here we need to create the advanced part.
				constructAdvancedTable(internal_unit);
				
			}
			//longpolling for the alive part?
			startPolling();			
		}
	});	
}
/****************************front page*********************/
function constructAdvancedTable(unit){
	var attr = unit.attributes;
	var values = {code:unit.code};
	
	values["ic_rpm"] = findAttribute(attr, "ic_rpm").value;
	values["oc_rpm"] = findAttribute(attr, "oc_rpm").value;
	
	var nodes = $("#template-advanced").find("input");
	for(var i = 0;i < nodes.length;i++){
		var name = $(nodes[i]).attr('name');
		var attrib = findAttribute(attr, name);
		if(attrib)
			values[name] = attrib.value;
		else(console.log(name));
	}
	$("#"+unit.code+"_setting").append(Mustache.render($("#template-advanced").html(), values));
	setToggles($("#"+unit.code+"_setting"), unit);
}

function setToggles(parent, unit){
	var attr = unit.attributes;
	//get all toggles (i)
	var vals = parent.find("i");
	for(var i = 0 ; i < vals.length; i++){
		var obj = $(vals[i]);
		var name = obj.attr("name");
		obj.removeClass("fa-toggle-on");
		var clazz = findAttribute(attr, name);

		clazz = (clazz.value && clazz.value === 'true')?"fa-toggle-on":"fa-toggle-off";
		obj.addClass(clazz);
		obj.on("click", function(){
			//send the value to the server here!!
			saveSetting(unit.code, $(this).attr("name"), $(this).hasClass("fa-toggle-off"));//happens before class is set. So reverse.
		});
	}
}
function constructUnit(unit){
	var trunk = getTrunk(unit);
	$("div.overview").append(trunk);
	createUnit(unit);
	
}

function getTrunk(unit){
	//get the html part
	var trunk = $("#templates").find("#frontpage").html();
	//fill necessary parameters
	var filled = Mustache.render(trunk, unit);
	return filled;
}

function createUnit(unit){
	doToggle(unit, "sys_on");
	doToggle(unit, "light_on");
	doToggle(unit, "ac_on");
	doToggle(unit, "humid_on");
	
	doGauge(unit, "in_temp","temp", "in");
	doGauge(unit, "pre_temp","temp", "pre");
	doGauge(unit, "on_temp","temp", "main");
	doGauge(unit, "humidity","humidity", "humid");
	doGauge(unit, "co2","co2", "co");
}

function doGauge(unit, attrib_name, gauge, meter){
	var name = unit.code;

	var attr = findAttribute(unit.attributes, attrib_name);
	if(!attr){
		attr={value:"NaN"};
	}
	var rest = name+"_"+meter;
	var canvas = $("canvas[name='"+rest+"']");
	switch(gauge){
		case "temp":
			gauges[name+"_"+attrib_name] = attrib_name==='in_temp'?createOutTempGauge(canvas, attr.value):createTempGauge(canvas, attr.value);
			break;
		case "humidity":
			gauges[name+"_"+attrib_name] = createHumidityGauge(canvas, attr.value);
			break;
		case "co2":
			gauges[name+"_"+attrib_name] = createCO2Gauge(canvas, attr.value);
			break;
	}
	$("div[name='"+name+"_"+meter+"_value']").find("span").html(attr.value);
}
	

function doToggle(unit, attrib_name){
	var name = unit.code;
	var on = findAttribute(unit.attributes, attrib_name);
	on = (on && on.value === 'true' );
	$("i[name='"+name+"_"+attrib_name+"']").toggleClass("fa-toggle-on", on);
	$("i[name='"+name+"_"+attrib_name+"']").toggleClass("fa-toggle-off", !on);
}

function startPolling(){
	(function poll() {
		setTimeout(function() {
			$.ajax({ 
				url: "rest/unit/poll?auth_ticket="+ticket, 
				success: function(data) {
					changeValues(data);
				}, 
				complete: poll
			});
		}, 2000);
	})();
}

function changeValues(data){
	var raw = JSON.parse(JSON.stringify(data));
	//take ticket and remove from data.
	storeTicket(raw);
	
	var units = raw.unit;
	
	if(!units)return;
	
	for(var i = 0; i < units.length;++i){
		var unit = units[i];
		var code = unit.code;
		var attribs = unit.attributes;
		//change gauges.
		var gKeys = Object.keys(gauges);
		
		for(var j = 0 ; j < gKeys.length;j++){
			var gKey = gKeys[j];
			for(var k = 0; k < attribs.length;k++){
				var toChk = code +"_"+attribs[k].code;
				if(toChk === gKey){
					gauges[gKey].set(attribs[k].value);
					$(gauges[gKey].canvas).next().find("span").html(attribs[k].value);
					break;
				}				
			}
		}
	}
	
}

function toggleUnitSwitch(unitParam){
	unitParam.toggleClass("fa-toggle-on");
	unitParam.toggleClass("fa-toggle-off");
}

/** For the gauge parts**/
function createOutTempGauge(gString, value){
	var zones = [
	   {strokeStyle: "#3881f7", min: -30, max: 0}, // Blue
	   {strokeStyle: "#FFDD00", min: 0, max: 15}, // Yellow
	   {strokeStyle: "#30B32D", min: 15, max: 25}, // Green
	   {strokeStyle: "#FFDD00", min: 25, max: 30}, // Yellow
	   {strokeStyle: "#F03E3E", min: 30, max: 40}  // Red
	];
	return createGauge(gString, value, -30, 40, zones);
}

function createTempGauge(gString, value){
	var zones = [
	   {strokeStyle: "#3881f7", min: 0, max: 10}, // Blue
	   {strokeStyle: "#FFDD00", min: 10, max: 15}, // Yellow
	   {strokeStyle: "#30B32D", min: 15, max: 25}, // Green
	   {strokeStyle: "#FFDD00", min: 25, max: 30}, // Yellow
	   {strokeStyle: "#F03E3E", min: 30, max: 50}  // Red
	];
	return createGauge(gString, value, 0, 50, zones);
}

function createHumidityGauge(gString, value){
	var zones = [
	   {strokeStyle: "#F03E3E", min: 0, max: 25}, //  Red
	   {strokeStyle: "#FFDD00", min: 25, max: 50}, // Yellow
	   {strokeStyle: "#30B32D", min: 50, max: 75}, // Green 
	   {strokeStyle: "#3881f7", min: 75, max: 100}, // Blue
	];
	return createGauge(gString, value, 0, 100, zones);
}

function createCO2Gauge(gString, value){
	var zones= [
		{strokeStyle: "#3881f7", min: 0, max: 400}, // Blue
		{strokeStyle: "#30B32D", min: 400, max: 1500}, // Green
		{strokeStyle: "#FFDD00", min: 1500, max: 3000}, // Yellow
		{strokeStyle: "#F03E3E", min: 3000, max: 5000}, // Red
	];
	return createGauge(gString, value, 0,5000, zones);
}

function createGauge(canvas, value, min, max, zones){
	var opts = {
		angle: -0.2, // The span of the gauge arc
		lineWidth: 0.2, // The line thickness
		radiusScale: 0.8, // Relative radius
		pointer: {
			length: 0.6, // // Relative to gauge radius
			strokeWidth: 0.035, // The thickness
			color: '#000000' // Fill color
		},
		limitMax: true,     // If false, max value increases automatically if value > maxValue
		limitMin: true,     // If true, the min value of the gauge will be fixed
		colorStart: '#6FADCF',   // Colors
		colorStop: '#8FC0DA',    // just experiment with them
		strokeColor: '#E0E0E0',  // to see which ones work best for you
		generateGradient: true,
		highDpiSupport: true,     // High resolution support
		staticZones: zones
	};
	var target = $(canvas).get(0); // your canvas element
	var gauge = new Gauge(target).setOptions(opts); // create gauge!
	gauge.maxValue = max; // set max gauge value
	gauge.setMinValue(min);  // Prefer setter over gauge.minValue = 0
	gauge.animationSpeed = 32; // set animation speed (32 is default value)
	gauge.set(value); // set actual value
	return gauge;
}