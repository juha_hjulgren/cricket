/**user.js contains all the functionality regarding users, doh.
**/
$(function(){	
	$("#user-button").on("click", function(){
		$("#old_pass").removeAttr('disabled');
		$("#username").prop('disabled', true);
		$("#usersettings").dialog("open");
		setUserSettings(user);
		
	});
	
	/*$("button[name='settings-user-ok']").on("click", function(){
		saveUser();
	});*/
	
	$("#settings-user-new").on("click", function(){
		//create a new accordion to the end of accordions
		$("#user-container").append(Mustache.render($("#template-users").html(), {username:'Uusi'}));
		$("#user-container").accordion("refresh");
		$("#user-container").accordion({
			active: -1
		});
		
		$("#settings_user_Uusi").find("input[name='old_pass']").prop('disabled', true);
		$("#settings_user_Uusi").find("input[name='username']").removeAttr('disabled');
		$("#settings_user_Uusi").find("input[name='username']").on("change", function(){
			
		});
		$("#settings_user_Uusi").find("button[name='settings-user-ok']").on("click", function(){
			saveUser("Uusi");
		});
	});
	
	$("button[name='settings-user-cancel']").on("click", function(){
		$("#usersettings").dialog("close");
	});
});

function testTicket(ticket){
	$.get("rest/login?auth_ticket="+ticket).done(function(data){
		if(data !== 'fail'){
			var raw = JSON.parse(data);
			//Take the ticket and delete from what server returned.
			ticket = raw.auth_ticket;
			sessionStorage.setItem("auth_ticket", ticket);
			delete raw.auth_ticket;
			//Also delete the hash, we don't want this here.
			delete raw.password;
			//put to user object
			user = raw;
			//as we have successful login lets get rid of the login window and load front page
			$("#login").dialog("close");
			showPage();
		}
	});
}

function setUserSettings(user){
	//get the users and construct accordion.
	$.get("rest/user/list?auth_ticket="+ticket).done(function(data){
		//get the template for rendering
		var template = $("#template-users").html();
		console.log(data);
		var raw = JSON.parse(JSON.stringify(data));
		//Take the ticket and delete from what server returned.
		ticket = raw.auth_ticket;
		sessionStorage.setItem("auth_ticket", ticket);
		delete raw.auth_ticket;
		var indx = 0;
		var i = 0;
		//loop users
		$.each(raw.user, function(){
			console.log(this);
			//create UI.
			$("#user-container").append(Mustache.render(template, this));
			//get the desired active index of accordion
			if(this.username === user.username)indx = i;
			++i;
		});
		$("#user-container").accordion({
			active: indx
		});
		
		//open current user.
		//if user is not admin disable user selection
		//prime so that when changes updates are made	
	});
	
	/*$("input#username").val(user.username);
	$("input#realname").val(user.realName);
	$("input#email").val(user.email);*/
}

function saveUser(frmName){
	var newPass = $("#settings_user_"+frmName).find("[name='new_pass']").val();
	var makeSure = $("#settings_user_"+frmName).find("[name='pass_again']").val();
	
	//check that new passwords match
	if((newPass !== makeSure)){
		return "password_mismatch";
	}
	
	//Check old password
	$("#settings_user_"+frmName).find("[name='username']").removeAttr('disabled');
	$("#settings_user_"+frmName).find("[name='old_pass']").removeAttr('disabled');
	//Send all to server.
	$.post(
		"rest/user", 
		$("#settings_user_"+frmName).serialize()+"&auth_ticket="+ticket
	).done(function(data){
		if(data !== 'fail'){
			var raw = JSON.parse(data);
			ticket = raw.auth_ticket;
			sessionStorage.setItem("auth_ticket", ticket);
			delete raw.auth_ticket;
			$("#settings_user_"+frmName).find("[name='username']").prop('disabled', true );
			return "save_success";
		}
	});	
}

function login(){
	//POST to server, with serialized form attached, when done store user json object.
	$.post("rest/login", $("#loginform").serialize()).done(function(data){
		var raw = JSON.parse(data);
		//add logon failed dialog here
		//Take the ticket and delete from what server returned.
		ticket = raw.auth_ticket;
		sessionStorage.setItem("auth_ticket", ticket);
		delete raw.auth_ticket;
		//Also delete the hash, we don't want this here.
		delete raw.password;
		//put to user object
		user = raw;
		//as we have successful login lets get rid of the login window and load front page
		$("#login").dialog("close");
		showPage();
	});
}

/**
Log out from the system. 
Empty the ticket, clear page.
**/
function logOut(){
	ticket = "";
	sessionStorage.removeItem("auth_ticket");
	$("#login").dialog("open");
	clearPage();
}

function createUser(){
	var data = $(this).closest("form").serialize();
	console.log(data);
	$.post("rest/user", data).done(function(data){
		//user created message or user creation failed.
	});
}

function clearPage(){
	$("div.contents").hide();
}

function showPage(){
	$("div.contents").show();
	initApp();
}

function checkForUserGroup(){
	return true;
	switch(user.usergroup){
		case "admin":
			//everything for u!
			break;
		case "user":
			//user gets to see history
			//user gets to see settings
			//user cannot change settings
			break;
		case "viewer":
			//disable top-left
			$("div.left_buttons").hide();
			//disable left
			$("div.left").hide();
			break;
	}
}

function checkForAdmin(){
	//temporarily, everyone is an admin.
	return true;
	//return (user.usergroup === "admin");
}
