function doUpload(fileObj){
	var formData = new FormData();
	formData.append('file', $(fileObj)[0].files[0]);
	formData.append('auth_ticket', ticket);
	formData.append('type', fileObj.id);
	$.ajax({
		   url : 'rest/file/',
		   type : 'POST',
		   data : formData,
		   processData: false,  // tell jQuery not to process the data
		   contentType: false,  // tell jQuery not to set contentType
		   success : function(data) {
			   console.log(data);
			   alert(data);
		   }
	});
}

function loadSecondCombo(first){
	var amt = first == "dbKontti1Vesiventtiilit"?14:20;
	
	var secSel = $("select[name='group_select']");
	secSel.html(""); 
	//console.log("loop groups "+amt);
	for(var i = 0; i < amt;i++){
		//console.log("loop group "+i);
		var key = "Ryhma"+(i+1);
		secSel.append($("<option></option>").attr("value",key).text(key));
	}
}

function saveAndStart(){
	$.post("rest/water/save",  $("#water_form").serialize()+"&auth_ticket="+ticket).done(function(data){ 
		var raw = JSON.parse(data);
		ticket = raw.auth_ticket;
		sessionStorage.setItem("auth_ticket", ticket);
		delete raw.auth_ticket;
	});
}

function loadWaterTimes(){
	var unit = $("select[name='unit_select']").val();
	var group = $("select[name='group_select']").val();
	//do the get
	
	$.get(
		"rest/water/read", 
		{
			"auth_ticket" : ticket, 
			"unit" : unit,
			"group" : group
		}
	).done(function(data){
		var raw = JSON.parse(JSON.stringify(data));
		ticket = raw.auth_ticket;
		sessionStorage.setItem("auth_ticket", ticket);
		delete raw.auth_ticket;
		$("input[name='open_value']").val(raw.open_value);
		$("input[name='close_value']").val(raw.close_value);
	});
}

function stopWater(){
	$.post("rest/water/stop", $("#water_form").serialize()+"&auth_ticket="+ticket).done(function(data){  
		var raw = JSON.parse(data);
		ticket = raw.auth_ticket;
		sessionStorage.setItem("auth_ticket", ticket);
		delete raw.auth_ticket;	
	});
}
