

function createChart(cUnit){
	var uid = cUnit.id;
	var code = cUnit.code;
	//console.log(cUnit);
	$.get(
		"rest/chart/day", 
		{
			"auth_ticket" : ticket, 
			"unit_id" : uid
		}
	).done(function(data){
		//console.log(data);
		var raw = JSON.parse(JSON.stringify(data));
		new Chart($("canvas[name='"+code+"_chart']"), {
		type: 'line',
		data: {
			labels:raw.labels,
			datasets: raw.datasets
		},
		options:{
			scaleShowValues: true,
			scales:{
				yAxes:[{
					ticks: {
						beginAtZero: true
					}
				}],
				xAxes: [{
					ticks: {
						autoSkip: false
					}
				}]
			},
			tooltips:{
				titleFontSize:0
			}
		}
	});
	});
}