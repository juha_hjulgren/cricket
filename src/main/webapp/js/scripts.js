$(function(){
	/*********start by declaring dialogs****/	
	//loginform base code
    $("#login").dialog({
		title: "Sisäänkirjautuminen",
		width:350,
		draggable: false,
		resizable: false,
		dialogClass: "no-close",
		modal: true,
		buttons: {
		}
	});
	
	$('#login').keypress(function(e) {
		if (e.keyCode == $.ui.keyCode.ENTER) {
			login();
		}
	});
	/*********startup code begins**************/
	//if localstorage has ticket
	ticket = sessionStorage.getItem("auth_ticket");
	//if ticket does not work, do login.
	if(ticket){
		//test ticket against server(to get the user)
		testTicket(ticket);	
	}
	else{
		clearPage();
	}
	
	//user settings base code
    $("#usersettings").dialog({
		title: "Käyttäjäasetukset",
		width:400,
		position: { my: 'top', at: '-200' },
		draggable: true,
		resizable: true,
		buttons: {
		}
	});
	$("#template-settings").dialog({
		title: "Asetukset",
		width:800,
		height:300,
		draggable: true,
		resizable: true,
		buttons: {
		},
		open: function(){
			parseSettings();
		},
	});
	
	$("#template-import-page").dialog({
		title:"Tuo profiilit",
		width:1200,
		height:600,
		draggable: true,
		resizable: true,
		buttons: {
		},
	});
	
	$("#import-progress").tabs();
	
	$("#template-import-page").dialog("close");
	$("#template-settings").dialog("close");
	$("#usersettings").dialog("close");
	
	//Temp
	//$("#login").dialog("close");
	//showPage();

/*******UI functionalities start from here *******************/
	
	
	$("#settings-button").on("click", function(){
		$("#template-settings").dialog("open");
	});
	
	$("#import-button").on("click", function(){
		//hide main page
		//show import page
		$("#template-import-page").dialog("open");
	});
	
	//Login button. Sends the serialized login form to server.
	$("button[name='login']").on("click", function(){
		login();
		initApp();
	});
	
	$("#signout-button").on("click", function(){
		logOut();
	});
	
	$("button.import").on("click", function(){
		doUpload($(this).closest("tr").find("input"));
	});
	
	/** watering stuff.**/
	$("select[name='unit_select']").on("change", function(){
		console.log("change combo data." +$(this).val());
		loadSecondCombo($(this).val());
	});	
	
	$("select[name='group_select']").on("change", function(){
		console.log("change group data." +$(this).val());
		loadWaterTimes($(this).val());
	});	
	
	$("button[name='save_and_start']").on("click", function(){
		saveAndStart();
	});
	
	$("button[name='stop_action']").on("click", function(){
		stopWater();
	});
});


