CREATE TABLE user(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,  
username VARCHAR(30) NOT NULL, 
realname VARCHAR(100) NOT NULL, 
email VARCHAR(50), 
password CHAR(40) NOT NULL,
usergroup ENUM('viewer', 'user', 'admin')
);
 
/**
username:	admin_cricket
password:	cricket
**/

INSERT INTO user(
	username, 
	realname, 
	email, 
	password, 
	usergroup
)
VALUES(
	'admin_cricket', 
	'Administrative Default User', 
	'admin@localhost', 
	'9613d18532e172fdc69f2cec8fdecc56e9a7fad2', 
	'admin'
);

CREATE TABLE tickets(
userid INT(6)UNSIGNED, 
ticket CHAR(36) NOT NULL, 
issuetime TIMESTAMP
);

CREATE TABLE unit(
id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(50) NOT NULL, 
code VARCHAR(30) NOT NULL, 
opcname VARCHAR(30) NOT NULL,
active CHAR(0)
);

CREATE TABLE attribute(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(50) NOT NULL,
code VARCHAR(30) NOT NULL,
opcname VARCHAR(50) NOT NULL,
value VARCHAR(255) NOT NULL,
type ENUM('string', 'boolean', 'float', 'time') NOT NULL,
editable CHAR(0)
);

CREATE TABLE unit_attribute(
unit_id INT(3) NOT NULL,
attrib_id INT(6) NOT NULL,
value VARCHAR(255) NOT NULL
);

CREATE TABLE event_log(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	unit_id INT(3), 
	attrib_id INT(6),
	message VARCHAR(255) NOT NULL,
	event_type ENUM('error', 'warning', 'message') NOT NULL,
	event_time TIMESTAMP,
	status CHAR(0),
	value VARCHAR(255)
);

CREATE TABLE attribute_history(
	unit_id INT(3), 
	attrib_id INT(6),
	event_time TIMESTAMP,
	value VARCHAR(255)
);

create table period(
	id INT(5),
	name VARCHAR(50)
);

create table day(
	id INT(5),
	name VARCHAR(50)
);

create table hour(
	id INT(5), 
	temp VARCHAR(20), 
	moist VARCHAR(20),
	brightness VARCHAR(20),
	water VARCHAR(20)
);

create table period_day(
	period_id INT(5),
	day_id INT(5),
	order_num INT(5)
);

create table day_hour(
	day_id INT(5), 
	hour_id INT(5),
	order_num, INT(5)
);

