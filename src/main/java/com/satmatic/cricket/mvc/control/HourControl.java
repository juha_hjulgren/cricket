package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.reader.xml.Hour;
import com.satmatic.cricket.server.CricketHelper;

public class HourControl extends DBControl {
	private Hour hour;
	
	public HourControl() {
		
	}
	
	public HourControl(Hour h) {
		this.setHour(h);
	}
	
	public void save() {
		String query = "SELECT 1 FROM hour WHERE id = "+this.getHour().getId();
		try {
			CricketHelper.getDB().findOne(query);
		}
		catch(NoDataException nde) {
			this.insert();
			return;
		}
		this.update();
	}
	
	private void update() {
		String insert = "update hour set("
			+ "temp = '"+this.hour.getTemp()+"', "
			+ "moist = '"+this.hour.getMoist()+"', "
			+ "brightness = '"+this.hour.getBrightness()+"', "
			+ "water = '"+this.hour.getWater()+"')"
			+ "where id = "+this.hour.getId();
			CricketHelper.getDB().execute(insert);
	}

	private void insert() {
		String insert = "insert into hour set("
			+ "id = '"+this.hour.getId()+"', "
			+ "temp = '"+this.hour.getTemp()+"', "
			+ "moist = '"+this.hour.getMoist()+"', "
			+ "brightness = '"+this.hour.getBrightness()+"', "
			+ "water = '"+this.hour.getWater()+"')";
			CricketHelper.getDB().execute(insert);
	}
	
	public Set<Hour> listAll(){
		String query = "SELECT * FROM hour";
		Set<Map<String, String>> data = CricketHelper.getDB().findAll(query);
		Set<Hour> toRet = new HashSet<Hour>();
		for(Map<String, String> mm : data){
			toRet.add(new Hour(mm));
		}
		return toRet;
	}
	
	public Hour findHour(int id) throws NoDataException {
		//select from database
		String query = "SELECT * FROM hour WHERE id = "+id;
		//parse as Hour
		Map<String, String> data = CricketHelper.getDB().findOne(query);
		//return hour
		return new Hour(data);
	}

	public Hour getHour() {
		return hour;
	}

	public void setHour(Hour hour) {
		this.hour = hour;
	}
}
