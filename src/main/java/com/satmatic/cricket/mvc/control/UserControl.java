package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.mvc.control.DBControl;
import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.model.User;
import com.satmatic.cricket.server.CricketHelper;

/**
 * 
 * @author JHjulgren
 * 
 * UserControl class is a class to interact with @User objects.
 *
 */
public class UserControl extends DBControl {
	
	private User user;
	
	private String userQuery = "SELECT id, username, realname, email, password, usergroup from user";
		
	public UserControl(User user) {
		this.user = user;
	}
	
	public UserControl(Map<String, String> userByTicket) {
		User u = new User(userByTicket);
		this.user = u;
	}

	public UserControl() {
	}

	public User findByUserName(String name) throws NoDataException {
		String query = userQuery +" WHERE username = '"+name+"'";
		Map<String, String> res;
		res = CricketHelper.getDB().findOne(query);
		
		this.user = new User(res);
		return this.user;
	}
	
	public void save() {
		if(user.getId() != null && Integer.parseInt(user.getId()) > 0) {
			this.update();
		}
		else {
			this.insert();
		}
	}
	
	public User getUser() {
		return this.user;
	}

	public Set<User> listAll() {
		Set<Map<String, String>> uu = CricketHelper.getDB().findAll(this.userQuery);
		Set<User> toRet = new HashSet<User>();
		for(Map<String, String> umap : uu) {
			toRet.add(new User(umap));
		}
		return toRet;
	}
	
	private void update() {
		String insert = "UPDATE user SET "+this.getCriterion()+" WHERE id = "+this.user.getId();
		CricketHelper.getDB().execute(insert);
		
	}
	
	private void insert() {
		String insert = "INSERT INTO user SET "+this.getCriterion();
		CricketHelper.getDB().execute(insert);
	}
	
	private String getCriterion() {
		String toRet = 
			"username = '"	+user.getUsername()+"',"+ 
			"realname = '"	+user.getRealName()+"',"+
			"email = '"		+user.getEmail()+"',"+
			"password ='"	+user.getPassWord()+"',"+
			"usergroup ='"	+user.getGroup()+"'";
		return toRet;
	}	
}
