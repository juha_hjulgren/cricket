package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.mvc.model.Event;
import com.satmatic.cricket.server.CricketHelper;

public class EventControl extends DBControl {
	
	private Event event;

	public Set<Event> findAll() {
		String query = "SELECT * from event_log";
		Set<Map<String, String>> data = CricketHelper.getDB().findAll(query);
		Set<Event> toRet = new HashSet<Event>();
		for(Map<String, String> mm : data) {
			toRet.add(new Event(mm));
		}
		return toRet;
	}
	
	public Set<Event> findByUnit(int unitId){
		String query = "SELECT * from event_log ";
		query += "WHERE unit_id = " +unitId;
		Set<Map<String, String>> data = CricketHelper.getDB().findAll(query);
		Set<Event> toRet = new HashSet<Event>();
		for(Map<String, String> mm : data) {
			toRet.add(new Event(mm));
		}
		return toRet;
	}

}
