package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.model.Attribute;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;

public class UnitControl extends DBControl {

	private Unit unit;
	
	public UnitControl() {
		
	}
	
	public UnitControl(Unit unit) {
		this.unit = unit;
	}
	
	public Set<Unit> getUnits(){
		String select = "SELECT * FROM unit";
		Set<Map<String, String>> units= CricketHelper.getDB().findAll(select);
		Set<Unit> toRet = new HashSet<Unit>();
		for(Map<String, String> mm : units) {
			Unit u = new Unit(mm);
			toRet.add(u);
		}
		return toRet;
	}
	 
	public void save(){
		String save, where = "";
		if(this.unit.getId() < 1) {
			String sel = "SELECT id from unit WHERE code = '"+this.unit.getCode()+"'";
			try {
				Map<String, String> m = CricketHelper.getDB().findOne(sel);
				this.unit.setId(Integer.parseInt(m.get("id")));
			} 
			catch (NoDataException e) {
				//Nothing to worry about
			}
			
		}
		
		if(this.unit.getId() > 0) {
			//update
			save = "UPDATE unit SET ";
			where = " WHERE id = "+unit.getId();
			
		}
		else {
			//create
			save = "INSERT INTO unit SET";
		}
		save +=  " code = '"+this.unit.getCode()+"',";
		save +=  " opcname = '"+this.unit.getOpcname()+"',";
		save +=  " name = '"+this.unit.getName()+"'";
		save += where;
		
		CricketHelper.getDB().execute(save);
		this.unit.setId(findByCode(this.unit.getCode()).getId());
		AttributeControl ac;
		for(Attribute a : this.unit.getAttributes()) {
			ac = new AttributeControl(this.unit, a);
			ac.save();
		}
		
		
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Unit findById(int unitId) {
		String query = "SELECT * from unit where id = "+unitId;
		try {
			Map<String, String> data = CricketHelper.getDB().findOne(query);
			this.unit = new Unit(data);
			return unit;
		} catch (NoDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public Unit findByCode(String unitCode) {
		String sel = "SELECT * FROM unit WHERE code = '"+unitCode+"'";
		try {
			Map<String, String> dta = CricketHelper.getDB().findOne(sel); 
			return new Unit(dta);
			
		} catch (NoDataException e) {
			//this should not occur.
		}
		
		return null;
	}
}
