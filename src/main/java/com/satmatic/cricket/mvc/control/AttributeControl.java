package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.model.Attribute;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;
import com.satmatic.cricket.server.DBConnector;

public class AttributeControl extends DBControl{
	
	final static Logger logger = Logger.getLogger(AttributeControl.class);
	
	private Unit unit;
	private Attribute attrib;

	
	public AttributeControl() {
	}
	
	public AttributeControl(Unit unit) {
		this.unit = unit;
	}

	public AttributeControl(Attribute a) {
		this();
		this.attrib = a;
	}
	
	public AttributeControl(Unit unit, Attribute a) {
		this(a);
		this.unit = unit;
	}
	
	public Set<Attribute> getAttributes() {
		String select = "SELECT a.id, a.name, a.code, a.opcname, b.value, a.type, a.editable "
				+ "FROM attribute a, unit_attribute b "
				+ "WHERE a.id = b.attrib_id "
				+ "AND b.unit_id = "+this.unit.getId();
		Set<Map<String, String>> data = CricketHelper.getDB().findAll(select);
		Set<Attribute> toRet = new HashSet<Attribute>();
		for(Map<String, String> mm : data){
			Attribute a = new Attribute(mm);
			toRet.add(a);
		}
		return toRet;
	}

	public Attribute getAttrib() {
		return attrib;
	}

	public void setAttrib(Attribute attrib) {
		this.attrib = attrib;
	}

	public void save() {
		String save;
		String where = "";
		if(this.attrib.getId() < 1) {
			String sel = "SELECT id from attribute where code='"+this.attrib.getCode()+"'";
			Map<String, String> m;
			try {
				m = CricketHelper.getDB().findOne(sel);
				this.attrib.setId(Integer.parseInt(m.get("id")));
			} 
			catch (NoDataException e) {
				//ok,. so we did not have this attribute in DB...
			}
		}
		
		if(this.attrib.getId() > 0) {
			save = "UPDATE attribute SET ";
			where = " WHERE id = " + this.attrib.getId();
		}
		else {
			save = "INSERT INTO attribute SET ";
		}
		save += " code = '"+this.attrib.getCode()+"', ";
		save += " name = '"+this.attrib.getName()+"', ";
		save += " opcname = '"+this.attrib.getOpcname()+"', ";
		
		save += " value = '"+this.attrib.getValue()+"', ";
		
		save += " type = '"+this.attrib.getType()+"', ";
		save += " editable = " + (this.attrib.isEditable()?"''":"NULL");
		save += where;
		
		CricketHelper.getDB().execute(save);
		this.attrib = findByCode(this.attrib.getCode());
		//Save the value to correct, so to speak location.
		if (this.unit != null) {
			connect(this.unit, this.attrib);
		}
	}

	public void connect(Unit u, Attribute a) {
		int uid = u.getId();
		int aid = a.getId();
		String select = "SELECT unit_id FROM unit_attribute WHERE ";
		select += " unit_id = "+uid;
		select += " AND attrib_id = "+aid;
		String insert = "INSERT INTO";
		String end = "";
		try {
			CricketHelper.getDB().findOne(select);
			insert = "UPDATE ";
			end = " WHERE ";
			end += " unit_id = "+uid;
			end += " AND attrib_id = "+aid;
		} catch (NoDataException e) {
			//These were not connected yet, so create.
		}
		insert += " unit_attribute SET";
		insert += " unit_id = "+uid+", ";
		insert += " attrib_id = "+aid+", ";
		insert += " value = '"+a.getValue()+"'";
		insert += end;
		
		CricketHelper.getDB().execute(insert);
	}

	public Attribute findByCode(String string) {
		String sel = "SELECT * FROM attribute WHERE code ='"+string+"'";
		try {
			Map<String, String> m = CricketHelper.getDB().findOne(sel);
			this.attrib = new Attribute(m);
			return this.attrib;
		} 
		catch (NoDataException e) {
			logger.error("Could not find attribute", e);
		}
		return null;
	}

	public Set<Attribute> getChilds(Attribute attribute) {
		String query = "select * from attribute where id IN(select child_id from attrib_attrib where parent_id = "+attribute.getId()+")";
		Set<Map<String, String>>ret = CricketHelper.getDB().findAll(query);
		Set<Attribute> toRet = new HashSet<Attribute>();
		for(Map<String, String> mm : ret) {
			Attribute aa = new Attribute(mm);
			toRet.add(aa);
		}
		return toRet;
	}

	public void setUnit(Unit uu) { 
		this.unit = uu;
	}

	public void archive() {
		String insert = "INSERT INTO attribute_history SET";
		insert += " unit_id = "+this.unit.getId()+",";
		insert += " attrib_id = "+this.attrib.getId()+",";
		insert += " value = '"+this.attrib.getValue()+"'";
		CricketHelper.getDB().execute(insert);
	}
}
