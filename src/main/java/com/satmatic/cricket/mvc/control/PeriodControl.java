package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.reader.xml.Period;
import com.satmatic.cricket.server.CricketHelper;

public class PeriodControl extends DBControl {
	private Period period;
	
	public PeriodControl() {
		
	}
	
	public PeriodControl(Period period) {
		this.setPeriod(period);
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}
	
	public Set<Period> listAll(){
		String query = "SELECT * FROM period";
		Set<Map<String, String>> per = CricketHelper.getDB().findAll(query);
		Set<Period> toRet = new HashSet<Period>();
		for(Map<String, String> mm : per) {
			Period p = new Period(mm);
			toRet.add(p);
		}
		return toRet;
	}
	
	public void save(){
		String test = "SELECT 1 from period where id = "+this.period.getId();
		try {
			CricketHelper.getDB().findOne(test);
			this.update();
		}
		catch(NoDataException nde) {
			this.insert();
		}
	}
	
	private void update(){
		String update = "UPDATE period set(name = '"+this.period.getName()+"') where id = "+this.period.getId();
		CricketHelper.getDB().execute(update);
	}
	
	private void insert() {
		String insert = "INSERT INTO period set("
				+ "id = "+this.period.getId()+", "
				+ "name = '"+this.period.getName()+"')";
		CricketHelper.getDB().execute(insert);
	}
}
