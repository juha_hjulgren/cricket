package com.satmatic.cricket.mvc.control;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.reader.xml.Day;
import com.satmatic.cricket.reader.xml.Hour;
import com.satmatic.cricket.server.CricketHelper;

public class DayControl extends DBControl {
	
	private Day day;

	public DayControl(Day d) {
		this.setDay(d);
	}

	public DayControl() {
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}
	
	public Set<Day> listAll(){
		String query = "Select * from day";
		Set<Map<String, String>> data = CricketHelper.getDB().findAll(query);
		Set<Day> toRet = new HashSet<Day>();
		Day d;
		for(Map<String, String>mm: data) {
			d = new Day(mm);
			d.createHours();
			toRet.add(d);
		}
		return toRet;
	}

	public void save() {
		String query = "SELECT 1 from day where id = "+this.day.getId();
		
		try {
			CricketHelper.getDB().findOne(query);
			this.update();
		}
		catch(NoDataException nde) {
			this.insert();
		}
		
		this.appendHours();
	}
	
	private void insert() {
		String insert = "INSERT into day set("
				+ "id = "+this.day.getId()
				+ "name = '"+this.day.getName()+"')";
		//also do the connections.
		
		CricketHelper.getDB().execute(insert);	
	}

	private void update() {
		String upd = "update day set(name='"+this.day.getName()+"') where id = "+this.day.getId();
		CricketHelper.getDB().execute(upd);	
	}
	
	private void appendHours() {
		//get all hours and make a join.
		this.day.createHours();
		List<Hour> hours = this.day.getHours();
		//save hours also if required.
		this.day.setHours(hours);
	}
	
}
