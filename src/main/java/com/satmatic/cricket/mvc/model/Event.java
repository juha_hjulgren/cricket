package com.satmatic.cricket.mvc.model;

import java.util.Map;

import com.satmatic.cricket.server.CricketHelper;

public class Event {
	public enum EVENT_TYPE {
	    ERROR, 
	    WARNING, 
	    INFO
	}
	
	private int id, unitId, attribId;
	private String message, eventTime, value;
	private EVENT_TYPE type;
	private boolean handled;
		
	public Event(int id, int unitId, int attribId, String message, String eventTime, EVENT_TYPE type, boolean handled, String value) {
		super();
		this.id = id;
		this.unitId = unitId;
		this.attribId = attribId;
		this.message = message;
		this.eventTime = eventTime;
		this.type = type;
		this.handled = handled;
	}
	
	public Event(Map<String, String >mm) {
		this.id = Integer.parseInt(mm.get("id"));
		this.unitId = CricketHelper.isNumber(mm.get("unit_id"))?Integer.parseInt(mm.get("unit_id")):0;
		this.attribId = CricketHelper.isNumber(mm.get("attrib_id"))?Integer.parseInt(mm.get("attrib_id")):0;
		this.message = mm.get("message");
		this.eventTime = mm.get("event_time");	
		this.handled =	mm.containsKey("handled");
		this.value = mm.get("value");
		switch(mm.get("event_type")) {
			case "error":
				this.type=EVENT_TYPE.ERROR;
				break;
			case "warning":
				this.type=EVENT_TYPE.WARNING;
				break;
			case "message":
				this.type=EVENT_TYPE.INFO;
				break;
		}
	}
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getUnitId() {
		return unitId;
	}
	
	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	
	public int getAttribId() {
		return attribId;
	}
	
	public void setAttribId(int attribId) {
		this.attribId = attribId;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getEventTime() {
		return eventTime;
	}
	
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
	public EVENT_TYPE getType() {
		return type;
	}
	
	public void setType(EVENT_TYPE type) {
		this.type = type;
	}
	
	public boolean isHandled() {
		return handled;
	}
	
	public void setHandled(boolean handled) {
		this.handled = handled;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
