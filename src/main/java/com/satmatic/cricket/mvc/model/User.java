package com.satmatic.cricket.mvc.model;

import java.util.Map;

/**
 * 
 * @author jhjulgren
 *
 *User class represents a user of the system. 
 *UserGroup tells what a User can do. 
 *User has a username, real name, email address and a group.
 */
public class User {
	private String id;
	private String username;
	private String realName;
	private String email;
	private String group;
	//transient means that we do not want this field to be serialized.
	private transient String password;
	
	public User(String id, String username, String realName, String email, String password, String group) {
		this.setId(id);
		this.username = username;
		this.realName = realName;
		this.email = email;
		this.password = password;
		this.group = group;
	}

	public User(Map<String, String> umap) {
		this(umap.get("id"), 
			umap.get("username"), 
			umap.get("realname"), 
			umap.get("email"), 
			umap.get("password"), 
			umap.get("group")
		);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	public void setPassword(String pass) {
		//TODO salt and save
		this.password = pass;
	}
	
	public String getPassWord() {
		return this.password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
