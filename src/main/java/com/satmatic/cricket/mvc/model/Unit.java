package com.satmatic.cricket.mvc.model;

import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.mvc.control.AttributeControl;
import com.satmatic.cricket.mvc.control.EventControl;

/**
 * 
 * @author jhjulgren
 * 
 * Unit class represents one Growing Unit.
 *
 */
public class Unit {
	private int id;
	private String name;
	private String code;
	private String opcname;
	private Set<Attribute> attributes;
	private Set<Event> events;

	public Unit(int id, String name, String code, String opcname) {
		super();
		this.id = id; 
		this.name = name;
		this.code = code;
		this.opcname = opcname;
		this.attributes = createAttributes();
		this.setEvents(createEvents());
	}

	public Unit(Map<String, String> mm) {
		this(Integer.parseInt(mm.get("id")), mm.get("name"), mm.get("code"), mm.get("opcname"));
	}

	public String getOpcname() {
		return opcname;
	}

	public void setOpcname(String opcname) {
		this.opcname = opcname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(Set<Attribute> attributes) {
		this.attributes = attributes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}
	
	private Set<Attribute> createAttributes() {
		AttributeControl ac = new AttributeControl(this);
		Set<Attribute> toRet = ac.getAttributes();
		return toRet;
	}
	
	private Set<Event> createEvents() {
		EventControl ec = new EventControl();
		Set<Event> toRet = ec.findByUnit(this.getId());
		return toRet;
	}
}
