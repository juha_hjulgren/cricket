package com.satmatic.cricket.mvc.model;

import java.util.Map;
import java.util.Set;

import com.satmatic.cricket.mvc.control.AttributeControl;

/**
 * 
 * @author jhjulgren
 * 
 * Attribute is one value for an unit, eg. temperature, humidity.
 *
 */
public class Attribute {

	private int id;				//internal id for the attribute
	private String code;		//code to be used to access attribute
	private String name;		//name to be shown in ui.
	private String opcname;		//what it's called in logic side
	private Object value;		//value in the raw form. basically a string.
	private String type;			//what is set in the enum above.
	private boolean editable;	//Can this value be edited
	
	private Set<Attribute> childs = null;
	
	public Attribute(Map<String, String> mm) {
		this.id = Integer.parseInt(mm.get("id"));
		this.code =	mm.get("code");
		this.name = mm.get("name");
		this.opcname = mm.get("opcname");
		this.value = mm.get("value");	
		this.editable =	mm.containsKey("editable");
		this.type = mm.get("type");
	
		/*if(this.hasChilds()) {
			//lazy loading...
			this.getChilds();
		}*/
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		if(this.getType().equals("float")) {
			double val = 0.00;
			try{
				val = Double.parseDouble(this.value+"");
			}
			catch(Exception any) {
				
			}
			this.value = String.format("%1.2f", val);
		}
		return this.value;
	}

	public void setValue(Object value) {
		if(this.getType().equals("float")) {
			
			double val = 0.00;
			try{
				val = Double.parseDouble(value+"");
			}
			catch(Exception any) {
				
			}
			this.value = String.format("%1.2f", val);
		}
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOpcname() {
		return opcname;
	}

	public void setOpcname(String opcname) {
		this.opcname = opcname;
	}
	
	public boolean hasChilds() {
		return 
			false;
	}
	
	/*public Set<Attribute> getChilds(){
		AttributeControl ac = new AttributeControl(this);
		this.childs = ac.getChilds(this);
		return this.childs;
	}*/
}
