package com.satmatic.cricket.mvc.model;

/**
 * 
 * @author jhjulgren
 * an enumeration (constants) for user groups.
 */
public enum UserGroup {
	VIEWER,
	USER,
	ADMIN
}
