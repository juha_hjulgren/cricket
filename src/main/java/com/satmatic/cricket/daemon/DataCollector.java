package com.satmatic.cricket.daemon;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * DataCollector is used to start a scheduled daemon that runs a thread on certain intervals, collectiong data from OPC-UA Server.
 * 
 * @author JHjulgren
 *
 */
public class DataCollector implements ServletContextListener {

    private ScheduledExecutorService scheduler;
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		scheduler = Executors.newSingleThreadScheduledExecutor();
	    Runnable command = new OPCThread(event.getServletContext());
	    // Delay 1 swecond to first execution
	    long initialDelay = 1;
	    TimeUnit unit = TimeUnit.SECONDS;
	    // period the period between successive executions
	    long period = 60;// 60 seconds!
	 
	    scheduler.scheduleAtFixedRate(command, initialDelay, period, unit);
		
	}

	
}
