package com.satmatic.cricket.daemon;

import java.io.FileNotFoundException;
import java.security.cert.CertificateException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import com.satmatic.cricket.mvc.control.AttributeControl;
import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.model.Attribute;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;
import com.satmatic.cricket.server.CricketSettings;
import com.satmatic.cricket.server.opc.OpcConnector;

/**
 * A thread to retrieve values from OPC-UA server.
 * 
 * @author JHjulgren
 *
 */
public class OPCThread implements Runnable {

	final static Logger logger = Logger.getLogger(OPCThread.class);
	
	private ServletContext context;
	private boolean debug = false;
	
	public OPCThread(ServletContext context) {
		this.context = context;
	}

	/**
	 * This method read units + attributes from DB and then retrieves all values from OPC-UA server.
	 * These values are then saved to the attributes and old values are saved into value history for graph use.
	 */
	@Override
	public void run() {
		try {
			if(this.debug) {
				//read units and attributes
				UnitControl uc = new UnitControl();
				Set<Unit> units = uc.getUnits();
				AttributeControl ac = new AttributeControl();
				String value;
				for(Unit uu : units) {
					Set<Attribute> attr = uu.getAttributes();
					ac.setUnit(uu);
					for(Attribute aa : attr) {
							ac.setAttrib(aa);
							ac.archive();
							value = getRandomValue(aa);
							aa.setValue(value);
							ac.setAttrib(aa);
							ac.save();
					}
				}
				CricketHelper.getDB().close();
				
			}
			else {
				//Connect to OPC-Server
				OpcConnector server = new OpcConnector();
				try {
					server.connect(CricketSettings.getInstance());
				} 
				catch (FileNotFoundException | InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					logger.error("could not connect to OPC-UA server!", e);
					e.printStackTrace();
				}
				//read units and attributes
				UnitControl uc = new UnitControl();
				Set<Unit> units = uc.getUnits();
				AttributeControl ac = new AttributeControl();
				for(Unit uu : units) {
					Set<Attribute> attr = uu.getAttributes();
					ac.setUnit(uu);
					for(Attribute aa : attr) {
						
						String opcName = "ns=3;s="+CricketHelper.parseUPCPath(uu.getOpcname()+"."+aa.getOpcname());
						try {
							String value = server.getNodeValue(NodeId.parse(opcName));
							//see if value changed?...
							//save to history
							ac.setAttrib(aa);
							ac.archive();
							
							aa.setValue(value);
							ac.setAttrib(aa);
							ac.save();
							
						} 
						catch (InterruptedException | ExecutionException e) {
							logger.error("Error reading value from OPC-UA server!", e);
							e.printStackTrace();
						}
					}
				}
				CricketHelper.getDB().close();
				server.release(CricketSettings.getInstance());
			}
		}
		catch(Exception any) {
			logger.error("Execution resulted in error!", any);
		}
	}

	private String getRandomValue(Attribute aa) {
		double rnd = Math.random();
		switch(aa.getType()) {
			case "float":
				return -(rnd * (0 - 100)) +"";
			case "boolean":
				return (rnd < 0.5) +"";
			default:
				return aa.getValue()+"";	
		}
	}
}
