package com.satmatic.cricket.reader.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "kontti")
@XmlAccessorType (XmlAccessType.FIELD)
public class Unit {
	
	//this is not in use
	private Period activePeriod;
	
	private int groupCount;
	
	@XmlElement(name="ryhma")
	private List<Group> groups; 
	
	@XmlAttribute(name = "id")
	private String opcName;

	public Period getActivePeriod() {
		return activePeriod;
	}

	public void setActivePeriod(Period activePeriod) {
		this.activePeriod = activePeriod;
	}

	public int getGroupCount() {
		return groupCount;
	}

	public void setGroupCount(int groupCount) {
		this.groupCount = groupCount;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public String getOpcName() {
		return opcName;
	}

	public void setOpcName(String opcName) {
		this.opcName = opcName;
	}

	public void addGroup(Group g) {
		if(this.groups == null) {
			this.groups = new ArrayList<Group>();
		}
		this.groups.add(g);
		
	}
}
