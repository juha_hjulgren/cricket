package com.satmatic.cricket.reader.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paivat")
@XmlAccessorType (XmlAccessType.FIELD)
public class Days {
	
	@XmlElement(name = "paiva")
	private List<Day> days;

	public List<Day> getDays() {
		return days;
	}

	public void setDays(List<Day> days) {
		this.days = days;
	}
	
	public void addDay(Day d) {
		if(this.days == null) {
			this.days = new ArrayList<Day>();
		}
		this.days.add(d);
	}
}
