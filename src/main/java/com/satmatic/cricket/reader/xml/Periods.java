package com.satmatic.cricket.reader.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "kasvatuskierrot")
@XmlAccessorType (XmlAccessType.FIELD)
public class Periods {
	
	@XmlElement(name = "kasvatuskierto")
	private List<Period> periods;

	public List<Period> getPeriods() {
		return periods;
	}

	public void setPeriods(List<Period> periods) {
		this.periods = periods;
	}
}
