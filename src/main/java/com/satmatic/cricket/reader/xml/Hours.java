package com.satmatic.cricket.reader.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tunnit")
@XmlAccessorType (XmlAccessType.FIELD)
public class Hours {
	
	@XmlElement(name = "tunti")
	private List<Hour> hours;

	public List<Hour> getHours() {
		return hours;
	}

	public void setHours(List<Hour> hours) {
		this.hours = hours;
	}
	
	public void addHour(Hour h) {
		if(this.hours == null) {
			this.hours = new ArrayList<Hour>();
		}
		this.hours.add(h);
	}
}
