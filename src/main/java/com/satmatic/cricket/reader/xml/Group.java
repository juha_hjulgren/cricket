package com.satmatic.cricket.reader.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ryhma")
@XmlAccessorType (XmlAccessType.FIELD)
public class Group {
	
	@XmlElement(name = "kasvatuskierto")
	private Period activePeriod;
	
	@XmlAttribute(name = "id")
	private int id;
	
	public Period getActivePeriod() {
		return activePeriod;
	}
	public void setActivePeriod(Period activePeriod) {
		this.activePeriod = activePeriod;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
