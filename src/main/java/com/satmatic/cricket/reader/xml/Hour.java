package com.satmatic.cricket.reader.xml;

import java.io.StringWriter;
import java.util.Map;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tunti")
@XmlAccessorType (XmlAccessType.FIELD)
public class Hour {
	
	@XmlAttribute(name="id")
	private int id;
	
	@XmlElement(name = "lampo")
	private double temp;
	
	@XmlElement(name = "kosteus")
	private int moist;
	
	@XmlElement(name = "kirkkaus")
	private int brightness;
	
	@XmlElement(name = "vesi")
	private int water;
	
	public Hour(Map<String, String> mm) {
		this.id = Integer.parseInt(mm.get("id"));
		this.temp = Double.parseDouble(mm.get("temp"));
		this.moist = Integer.parseInt(mm.get("moist"));
		this.brightness = Integer.parseInt(mm.get("brightness"));
		this.water = Integer.parseInt(mm.get("water"));
	}

	public double getTemp() {
		return temp;
	}
	
	public void setTemp(double temp) {
		this.temp = temp;
	}
	
	public int getMoist() {
		return moist;
	}
	
	public void setMoist(int moist) {
		this.moist = moist;
	}
	
	public int getBrightness() {
		return brightness;
	}
	
	public void setBrightness(int brightness) {
		this.brightness = brightness;
	}
	
	public int getWater() {
		return water;
	}
	
	public void setWater(int water) {
		this.water = water;
	}
	
	//TODO this will always be the same, mostly, probably.
	@Override
	public String toString() {
		StringWriter sw = new StringWriter();
		JAXB.marshal(this, sw);
		return sw.toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
