package com.satmatic.cricket.reader.xml;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "kasvatuskierto")
@XmlAccessorType (XmlAccessType.FIELD)
public class Period {
	
	@XmlAttribute(name="id")
	private int id;
	
	@XmlElement(name = "nimi")
	private String name;
	
	@XmlElement(name = "pituus")
	private int length;
	
	@XmlElement(name = "maara")
	private int dayCount;
	
	@XmlElement(name = "paivat")
	private Days days;

	public Period(Map<String, String> mm) {
		this.id = Integer.parseInt(mm.get("id"));
		this.name = mm.get("name");
	}

	public Period() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getDayCount() {
		return dayCount;
	}

	public void setDayCount(int dayCount) {
		this.dayCount = dayCount;
	}

	public Days getDays() {
		return days;
	}

	public void setDays(Days days) {
		this.days = days;
	}

	public void addDay(Day d) {
		if(this.days == null) {
			this.days = new Days();
		}
		this.days.addDay(d);
	}
}
