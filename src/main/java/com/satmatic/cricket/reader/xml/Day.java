package com.satmatic.cricket.reader.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.satmatic.cricket.server.CricketHelper;

@XmlRootElement(name = "paiva")
@XmlAccessorType (XmlAccessType.FIELD)
public class Day { 

	@XmlAttribute(name="id")
	private int id;
	
	@XmlElement(name = "nimi")
	private String name;
	
	@XmlElement(name = "tunti")
	private List<Hour> hours;
	
	public Day(Map<String, String> mm) {
		this.id = Integer.parseInt(mm.get("id"));
		this.name = mm.get("name");
	}

	public int getId() { 
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Hour> getHours() {
		return this.hours;
	}
	
	public void setHours(List<Hour> hours) {
		this.hours = hours;
	}

	public void addHour(Hour h) {
		if(this.hours == null) {
			this.hours = new ArrayList<Hour>();
		}
		this.hours.add(h);	
	}
	
	public void createHours() {
		String query = "SELECT b.* FROM day_hour a, hour b where b.id =  and a.day_id = "+this.id+" ORDER_BY a.order_num";
		
		Set<Map<String, String>> data = CricketHelper.getDB().findAll(query);
		
		for(Map<String, String> mm : data) {
			this.addHour(new Hour(mm));
		}
	}
}
