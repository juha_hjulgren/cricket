package com.satmatic.cricket.reader.xml;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "kontit")
@XmlAccessorType (XmlAccessType.FIELD)
public class Units {
	
	@XmlElement(name="kontti")
	private Set<Unit> units;
	
	public Set<Unit> getUnits() {
		return units;
	}

	public void setUnits(Set<Unit> units) {
		this.units = units;
	}

	public void addUnit(Unit uu) {
		if(this.units == null) {
			this.units = new HashSet<Unit>();
		}
		this.units.add(uu);
	}
}
