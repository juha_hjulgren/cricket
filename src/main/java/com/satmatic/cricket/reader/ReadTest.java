package com.satmatic.cricket.reader;

import java.net.URL;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.satmatic.cricket.mvc.control.HourControl;
import com.satmatic.cricket.reader.xml.Day;
import com.satmatic.cricket.reader.xml.Days;
import com.satmatic.cricket.reader.xml.Group;
import com.satmatic.cricket.reader.xml.Hour;
import com.satmatic.cricket.reader.xml.Hours;
import com.satmatic.cricket.reader.xml.Period;
import com.satmatic.cricket.reader.xml.Unit;
import com.satmatic.cricket.reader.xml.Units;

public class ReadTest {

	public static void main(String [] args) throws JAXBException {
		//open file. 
		//need to construct like a master file
		//create an xml file
		ReadTest rt = new ReadTest();
		rt.testWriting();
		rt.testReading();
	}
	
	private void testReading() {
		ClassLoader classLoader = getClass().getClassLoader();
		
		URL hour, day, period, unit;
	 
		//read hours
		hour = classLoader.getResource("hours.xml");
		Hours hours = JAXB.unmarshal(hour, Hours.class);
		System.out.println(hours);
		HourControl hc = new HourControl();
		for(Hour h : hours.getHours()) {
			hc.setHour(h);
			hc.save();
		}
		//read days
		day = classLoader.getResource("days.xml");
		Days days = JAXB.unmarshal(day, Days.class);
		System.out.println(hours);
		//read periods
		
		//read units
		
	}

	/**
	 * Test outputting xml to see that the structure is correct.
	 * @throws JAXBException 
	 */
	private void testWriting() throws JAXBException {
		Units u = new Units();
		Unit uu = new Unit();
		uu.setOpcName("Kontti 1");
		u.addUnit(uu);
		Group g = new Group();
		g.setId(1);
		uu.addGroup(g);
		Period p = new Period();
		p.setId(1);
		g.setActivePeriod(p);
		/*Day d = new Day();
		d.setId(1);
		p.addDay(d);
		Day d2 = new Day();
		d2.setId(2);
		p.addDay(d2);
		Hour h = new Hour();
		h.setId(1);
		h.setBrightness(80);
		h.setMoist(60);
		h.setTemp(32.0);
		h.setWater(200);
		d.addHour(h);*/
		JAXBContext jaxbContext = JAXBContext.newInstance(Units.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	     
	    //Marshal the employees list in console
	    jaxbMarshaller.marshal(u, System.out);
	}
}
