package com.satmatic.cricket;

/**
 * If no data is found, well. The historic way of dealing is to use an exception. So that is what i used.
 * @author JHjulgren
 *
 */
public class NoDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "no_data_found";
	}

	
}
