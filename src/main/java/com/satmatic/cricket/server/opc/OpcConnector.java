package com.satmatic.cricket.server.opc;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfigBuilder;
import org.eclipse.milo.opcua.sdk.client.api.nodes.Node;
import org.eclipse.milo.opcua.sdk.client.api.nodes.VariableNode;
import org.eclipse.milo.opcua.stack.client.UaTcpStackClient;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.satmatic.cricket.server.CricketSettings;

/** 
 * 
 * @author jhjulgren
 *
 *A class that will serve as connection point between WS and Logic.
 */
public class OpcConnector {
	
	private OpcUaClient client;
	
	private Logger log = LoggerFactory.getLogger(OpcConnector.class);
	
	public OpcConnector() {
	}
	
	/** 
	 * Try connecting to OPC Server. 
	 * 
	 * @param settings CricketSettings instance
	 * 
	 * @throws FileNotFoundException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public void connect(CricketSettings settings) throws FileNotFoundException, InterruptedException, ExecutionException {
		log.debug("Connect to UPC UA");
		String opc_addr = CricketSettings.getInstance().getParam("opc_server_address");
		
		EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints(opc_addr).get();
		
		//String endpointUrl = endpoints[1].getEndpointUrl();//settings.getParam("opc_server_address");
		OpcUaClientConfigBuilder config = new OpcUaClientConfigBuilder();
		
		config.setEndpoint(endpoints[0]);
		//config.setEndpointUrl(endpointUrl);
		
		this.client = new OpcUaClient(config.build());
		this.client.connect().get();
		
		log.debug("Connect done");
	}
	
	public void debugNodes(String indent, NodeId browseRoot) {
		try {
			List<Node> nodes = this.client.getAddressSpace().browse(browseRoot).get();
	        for (Node node : nodes) {
	        	String name = node.getBrowseName().get().getName();
	        	String cmpr = name.replace("Node=", "");
	        	if(name.contains("Node=Server"))continue;
	        	try{
	        		Integer.parseInt(cmpr);
	        	}
	        	catch(Exception any) {
	        		System.out.println(indent+" Node="+name);
	        	}
	        	finally {
	                // recursively browse to children
		        	debugNodes(indent + "  ", node.getNodeId().get());
	        	}
	        }
	    } 
		catch (InterruptedException | ExecutionException e) {
	    	e.printStackTrace();
	    }
	}
	
	/**
	 * 	Get a single node value from server.
	 * 
	 * @param nodeId id of the node to be returned
	 * @return String representation of the value
	 * 
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public String getNodeValue(NodeId nodeId) throws InterruptedException, ExecutionException {
		log.debug("Get node value from OPC UA");
		VariableNode node= client.getAddressSpace().createVariableNode(nodeId);
        DataValue value = node.readValue().get();
        
        String val = value.getValue().getValue()+"";
        log.debug("Value obtained");
        return val;
	}
	
	/** Set a node value to server.
	 *  Please note that everything has to be precise before calling this method.
	 *  
	 * @param nodeId id of the node of which value is wanted to be changed.
	 * @param value the precise, correct type value to set
	 * @return success.
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public boolean setNodeValue(NodeId nodeId, Object value) throws InterruptedException, ExecutionException {
		System.out.println("Setting value across OPC UA");
		StatusCode sc = this.client.writeValue(nodeId, DataValue.valueOnly(new Variant(value))).get();
		System.out.println("server returned status code "+sc.toString());
		return sc.isGood();
	}

	public void release(CricketSettings instance) {
		this.client.disconnect();
	}
}
