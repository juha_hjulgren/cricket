 package com.satmatic.cricket.server.opc;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfigBuilder;
import org.eclipse.milo.opcua.sdk.client.api.nodes.Node;
import org.eclipse.milo.opcua.sdk.client.api.nodes.VariableNode;
import org.eclipse.milo.opcua.stack.client.UaTcpStackClient;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;

import com.satmatic.cricket.server.CricketHelper;
import com.satmatic.cricket.server.CricketSettings;

/**
 * This class was used to test the OPC-UA connections. fetching and saving.
 * 
 * @author JHjulgren
 *
 */
public class OPCTester {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		String opc_addr = CricketSettings.getInstance().getParam("opc_server_address");
		
		EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints(opc_addr).get();
		
		OpcUaClientConfigBuilder cfg = new OpcUaClientConfigBuilder();
		cfg.setEndpoint(endpoints[0]);
		
		OpcUaClient client = new OpcUaClient(cfg.build());
		client.connect().get();
		
		 // start browsing at root folder
        browseNode("", client, Identifiers.RootFolder);
		
//		String unit = "Kontti 1";
//		String attrib = "Lampotila1.PV";
//		String unParsed = unit+"."+attrib;
//		String parsed = CricketHelper.parseUPCPath(unParsed);		
//		String nodeString = "ns=3;s="+parsed;
//		
//		VariableNode node= client.getAddressSpace().createVariableNode(NodeId.parse(nodeString));
//        DataValue value = node.readValue().get();
//		
//        unParsed = unit+".IlmP��lle";
//		parsed = CricketHelper.parseUPCPath(unParsed);
//		nodeString = "ns=3;s="+parsed;
//        client.writeValue(NodeId.parse(nodeString), DataValue.valueOnly(new Variant(true))).get();
		
		//DataValue value = client.readValue(0, TimestampsToReturn.Both, nodeId).get();
	}
	
	public static void browseNode(String indent, OpcUaClient client, NodeId browseRoot) {
		try {
			List<Node> nodes = client.getAddressSpace().browse(browseRoot).get();
	        for (Node node : nodes) {
	        	System.out.println(indent+" Node="+node.getBrowseName().get().getName());
                // recursively browse to children
	            browseNode(indent + "  ", client, node.getNodeId().get());
	        }
	    } 
		catch (InterruptedException | ExecutionException e) {
	    	e.printStackTrace();
	    }
	}
}
