package com.satmatic.cricket.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/** 
 * 
 * @author jhjulgren
 *
 *   a Singleton class for reading/writing settings of the application.
 *	 A singleton pattern is a pattern where one and only one instance of a certain object "lives" in any given time. 
 *   Realization will follow once we know what type of settings for the program are required.
 */
public class CricketSettings {
	private static CricketSettings settings;
	private Properties cricketProps = new Properties();
	
	final static Logger logger = Logger.getLogger(CricketSettings.class);
	
	/**
	 * Private constructor, so no instances can be created.
	 * @throws IOException If properties cannot be read.
	 */
	private CricketSettings() throws IOException {
		//first, load .properties file
		InputStream input = null;
		ClassLoader classLoader = getClass().getClassLoader();
		
		input = new FileInputStream(classLoader.getResource("cricket.properties").getFile());
		// load a properties file
		cricketProps.load(input);

		/**
		 * What properties do we have?
		 * 
		 *database name
		 *mysql username
		 *mysql password
		 *
		 */
		//load rest of the properties from database
		loadDBProps();
	}
	
	private void loadDBProps()  {
		//TODO load from db and append to properties.
		/*DBConnector db = CricketHelper.getDB();
		db.connect(this);
		Set<Map<String, String>> set = db.findAll("SELECT key, value FROM settings");
		for(Map<String, String> m : set) {
			this.cricketProps.setProperty(m.get("key"), m.get("value"));
		}*/
	}

	/**
	 * 	This method gives you an instance of the settings class.
	 * 
	 * @return an instance of cricketSettings.
	 */
	public static CricketSettings getInstance() {
		if(settings == null) {
			try {
				settings = new CricketSettings();
			} catch (IOException e) {
				logger.error("Error when loading properties", e);
				e.printStackTrace();
			}
		}
		return settings;
	}
	
	/**
	 *  Get a certain parameter from settings.
	 * @param paramName name to look for
	 * @returnString parameter requested.
	 */
	public String getParam(String paramName) {
		return cricketProps.getProperty(paramName);
	}
}
