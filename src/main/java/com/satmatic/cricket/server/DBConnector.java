package com.satmatic.cricket.server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.satmatic.cricket.NoDataException;

/**
 * 
 * This class is a Connecting class for MySQL Database.
 * It contains the basic functionality for connection, select and execute.
 * 
 * @author JHjulgren
 *
 */
public class DBConnector {
	private MysqlDataSource srv;
	
	final static Logger logger = Logger.getLogger(DBConnector.class);
	
	public DBConnector() {
		
	}
	
	/**
	 * Connect to MySQL Database. Settings should include necessary parameters.
	 * 
	 * @param settings An instance of @com.satmatic.cricket.server.CricketSettings that contains all necessary parameters.
	 */
	public void connect(CricketSettings settings) {
		this.srv = new MysqlDataSource();
		//Do we need more protection, really? 
		//User is only allowed to connect directly from the computer. Or SSH... There are quite many protection layers in place.
		this.srv.setUser(CricketSettings.getInstance().getParam("db_user"));
		this.srv.setPassword(CricketSettings.getInstance().getParam("db_pass"));
		this.srv.setServerName(CricketSettings.getInstance().getParam("server_address"));
		/**
		 * this.srv.setServerName(settings.getParam(CricketHelper.SERVERNAME));
		 */
		this.srv.setDatabaseName(CricketSettings.getInstance().getParam("db_name"));
	}
	
	public void close() {
		try {
			this.srv.getConnection().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Select one row from database based on the given query. 
	 * @param query to be executed
	 * @return Map containing all values in the row.
	 * @throws NoDataException  throws an exception with text "no_data" if there are no rows. 
	 *
	 */
	public Map<String, String> findOne(String query) throws NoDataException {
		PreparedStatement stmt;
		try {
			stmt = this.srv.getConnection().prepareStatement(query);
			stmt.execute();
			ResultSet res = stmt.getResultSet();
			ResultSetMetaData rsmd = stmt.getMetaData();
			if(!res.next()) {
				throw new NoDataException();
			};//only one row.
			Map<String, String> toRet = new HashMap<String, String>();
			for(int i = 1; i <= rsmd.getColumnCount();i++) {
				String colName = rsmd.getColumnName(i);
				String colData = res.getObject(i) + "";//autoboxing to the rescue
				toRet.put(colName,  colData);
			}
			//This should be the route where we return the results....
			return toRet;
			
		} catch (SQLException e) {
			logger.error("Query threw an exception", e);
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * Select multiple rows from database.
	 * @param query to be executed
	 * @return a Set of rows containing Maps containing all values in the row.
	 */
	public Set<Map<String, String>> findAll(String query)  {
		try {
			PreparedStatement stmt = this.srv.getConnection().prepareStatement(query);
			stmt.execute();
			ResultSet res = stmt.getResultSet();
			ResultSetMetaData rsmd = stmt.getMetaData();
			Set<Map<String, String>> toRet = new HashSet<Map<String, String>>();
			while(res.next()) {
				Map<String, String> row = new HashMap<String, String>();
				for(int i = 1; i <= rsmd.getColumnCount();++i) {
					String colName = rsmd.getColumnLabel(i);
					String colData = res.getObject(i) + "";//autoboxing to the rescue
					row.put(colName,  colData);
				}
				toRet.add(row);
			}
			return toRet;
		} catch (SQLException e) {
			logger.error("Query threw an exception", e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Execute a statement that is not expected to really... return anything. 
	 * @param sql Insert, update, delete statement.
	 */
	public void execute(String sql){
		PreparedStatement stmt;
		try {
			stmt = this.srv.getConnection().prepareStatement(sql);
			stmt.execute(); 
		} catch (SQLException e) {
			logger.error("Execution threw an exception", e);
			e.printStackTrace();
		}
		
	}
	
	
}
