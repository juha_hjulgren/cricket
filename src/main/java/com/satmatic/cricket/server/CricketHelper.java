package com.satmatic.cricket.server;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.mvc.model.User;

/**
 * 
 * @author jhjulgren
 * 
 * A class for static helper methods used all around the system.
 *
 */
public class CricketHelper {
	
	private static final String SALT = 			"SatMatic_cricket";
	public static final String ADMIN_EMAIL = 	"admin_email";
	public static final String DEFAULT_PASS = 	"cricket";
	public static final String SERVERNAME = 	"server";
	
	private static DBConnector DB;

	/**
	 * get the connection to MySql Database
	 * @return DBConnector object
	 */
	public static DBConnector getDB() {
		if(CricketHelper.DB == null) {
			CricketHelper.DB = new DBConnector();
			CricketHelper.DB.connect(CricketSettings.getInstance());
		}
		return CricketHelper.DB;
	}
	
	/**
	 * Check a ticket, if it works obtain a new ticket as return. 
	 * One of the quirks when using stateless services. So we keep the state in the client side.
	 * @param u user object to check against.
	 * @param ticket the ticket o be checked for.
	 * @return a new ticket to be returned to the client.
	 */
	public static String checkTicket(User u, String ticket) {
		DBConnector db = CricketHelper.getDB();
		//check that ticket matches.
		String select = "SELECT ticket, issuetime FROM tickets WHERE " + 
				"userid = "+ u.getId()+" AND "+ 
				"ticket = '"+ticket+"')";
		Map<String, String> row;
		try {
			row = db.findOne(select);
		} 
		catch (NoDataException e) {
			return "fail";
		}
		return CricketHelper.obtainTicket(u);
	}
	
	/**
	 * Create and return a new unused ticket. Tickets are one time use UUID Strings. Once it's used it's gone.
	 * @param u User to whom to create a ticket to.
	 * @return UUID String representation.
	 */
	public static String obtainTicket(User u) {
		DBConnector db = CricketHelper.getDB();
		String ticket = UUID.randomUUID().toString();
		//save userid, ticket to database.
		String insert = "INSERT INTO tickets(userid, ticket) VALUES (";
		insert += u.getId();
		insert += ", '"+ticket+"')";
		db.execute(insert);
		deleteStaleTickets();
		return ticket;
	}

	/**
	 * This is a helper method for hashing passwords.
	 * 
	 * @param input String to create a Hash from.
	 * @return hashed String
	 */
	public static String generateHash(String input) {
		StringBuilder hash = new StringBuilder();
		//append salt to the string.
		input = CricketHelper.SALT + input;
		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(input.getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'a', 'b', 'c', 'd', 'e', 'f' };
			for (int idx = 0; idx < hashedBytes.length; ++idx) {
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}
		} catch (NoSuchAlgorithmException e) {
			// handle error here.
		}

		return hash.toString();
	} 
	
	/** 
	 * Parse Object to JSON String and append the ticket.
	 * @param toParse Object to parse
	 * @param ticket Ticket UUID.
	 * @return parsed object string.
	 */
	public static String parseJsonReturn(Object toParse, String ticket, String objName) {
		Gson gson = new Gson();
		JsonElement jsonElement = gson.toJsonTree(toParse);
		JsonObject tmp = new JsonObject();
		if (jsonElement instanceof JsonObject) {
			tmp = jsonElement.getAsJsonObject();
			//TODO need to wrap this in "data" as with arrays to be consistent.
			tmp.addProperty("auth_ticket", ticket);
			
		} 
		else if (jsonElement instanceof JsonArray) {
			 jsonElement = jsonElement.getAsJsonArray();
			 tmp.add(objName, jsonElement);
			 tmp.addProperty("auth_ticket", ticket);
		}
		else if (jsonElement instanceof JsonPrimitive) {
			Map m = new HashMap();
			m.put(objName, toParse);
			return parseJsonReturn(m, ticket, objName);
		}

		return tmp.toString();
	}
	
	
	//FIXME: this needs to be thought through
	public static Set<Unit> readFromFile(String file) throws FileNotFoundException{
		Type UNIT_TYPE = new TypeToken<Set<Unit>>() {}.getType();
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader(file));
		Set<Unit> toRet = gson.fromJson(reader, UNIT_TYPE);
		return toRet;
	}

	/**
	 * Resolve the user asking info from the ticket object. As the ticket is bound to user in our DB this is quite trivial.
	 * @param ticket the ticket string to use for resolving.
	 * @return Map of Strings containing user data from the DB.
	 */
	
	public static Map<String, String> getUserByTicket(String ticket) {
		DBConnector db = CricketHelper.getDB();
		//check that ticket matches.
		String select = "SELECT userid, ticket, issuetime FROM tickets WHERE " + 
				"ticket = '"+ticket+"'";
		Map<String, String> row = null;
		try {
			row = db.findOne(select);
		} catch (NoDataException e1) {
			//TODO log exception.
		}
		
		//get the user details*
		select = "SELECT * FROM user WHERE id = "+row.get("userid");
		try {
			row = db.findOne(select);
		} catch (NoDataException e) {
			//TODO log exception.
		}
		return row;
	}
	
	/**
	 * See if the string given is in fact a working number.
	 * @param s string to check
	 * @return true if number, false otherwise.
	 */
	public static boolean isNumber(String s) {
		boolean toRet = s.chars().allMatch( Character::isDigit );
		return toRet;
	}

	public static String parseUPCPath(String unParsed) {
		String[] ss = unParsed.split("\\.");
		String toRet = "\""+String.join("\".\"", ss)+"\"";
		return toRet;
	}
	
	/**
	 * delete all tickets older than 2 minutes. If a session is active (page open) polling keeps session running anyways. 
	 */
	private static void deleteStaleTickets() {
		String delete = "DELETE FROM tickets where issuetime < (NOW() - INTERVAL 2 MINUTE)";
		CricketHelper.getDB().execute(delete);
	}
}
