/**
 * 
 */
package com.satmatic.cricket.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;

import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.model.Unit;

/**
 * This class was created to test reading Units+attributes from a .json file.
 * 
 * @author JHjulgren
 *
 */
public class InitTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		InitTest it = new InitTest();
		it.createUnits();
	}

	private void createUnits() {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			String fname = classLoader.getResource("attrib_descr.json").getFile().toString();
			Set<Unit> units = CricketHelper.readFromFile(fname);
			UnitControl uc = null; 
			for(Unit uu :units) {
				uc = new UnitControl(uu);
				uc.save();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
