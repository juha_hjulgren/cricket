package com.satmatic.cricket.server;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.satmatic.cricket.rest.ChartDataService;
import com.satmatic.cricket.rest.EventService;
import com.satmatic.cricket.rest.FileService;
import com.satmatic.cricket.rest.LoginService;
import com.satmatic.cricket.rest.OpcService;
import com.satmatic.cricket.rest.SettingsService;
import com.satmatic.cricket.rest.SimpleWatering;
import com.satmatic.cricket.rest.SysInit;
import com.satmatic.cricket.rest.UnitService;
import com.satmatic.cricket.rest.UserService;


/**
 * 
 * @author JHjulgren
 * 
 * This class is to map all available Rest services in this application.
 *
 */

@ApplicationPath("rest")
public class CricketServer extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(LoginService.class);
        s.add(UserService.class);
        s.add(EventService.class);
        s.add(UnitService.class);
        s.add(ChartDataService.class);
        s.add(SettingsService.class);
        s.add(OpcService.class);
        s.add(SysInit.class);
        s.add(SimpleWatering.class);
        return s;
    }
}