package com.satmatic.cricket.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.mvc.model.User;
import com.satmatic.cricket.server.CricketHelper;

@Path("settings")
public class SettingsService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getSettings(@QueryParam(value = "auth_ticket") String ticket) {
		//get a new ticket
		UserControl usc = new UserControl(CricketHelper.getUserByTicket(ticket));
		ticket = CricketHelper.checkTicket(usc.getUser(), ticket);
		
		//TODO this makes no sense at the moment... get settings from db
		String set = "select key, value from settings";
		Set<Map<String, String>> res = CricketHelper.getDB().findAll(set);
		//get units from db
		UnitControl uc = new UnitControl();
		Set<Unit> units = uc.getUnits();
		//parse together
		Map<String, Object> dta = new HashMap<String, Object>();
		for(Map<String, String> mm : res) {
			dta.putAll(mm);
		}
		dta.put("units", units);
		
		CricketHelper.getDB().close();
		
		//return everything
		return CricketHelper.parseJsonReturn(dta, ticket,"settings");
	}
}
 