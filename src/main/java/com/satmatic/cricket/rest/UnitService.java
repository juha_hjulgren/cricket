package com.satmatic.cricket.rest;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;


@Path("unit")
public class UnitService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/list")
	public String listUnits(@QueryParam(value = "auth_ticket") String ticket) {
		//check ticket
		//TODO we always check for ticket and grant a new ticket, is there a more efficient way?
		UserControl user = new UserControl(CricketHelper.getUserByTicket(ticket));
		
		UnitControl uc = new UnitControl();
		Set<Unit> units = uc.getUnits();
		
		//obtain new ticket
		ticket = CricketHelper.obtainTicket(user.getUser());
		CricketHelper.getDB().close();
		return CricketHelper.parseJsonReturn(units,  ticket, "unit");
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/poll")
	public String pollUnits(@QueryParam(value = "auth_ticket") String ticket) {
		long start = System.currentTimeMillis();
		UserControl user = new UserControl(CricketHelper.getUserByTicket(ticket));
		long now;
		ticket = CricketHelper.obtainTicket(user.getUser());
		while(true) {
			now = System.currentTimeMillis();
			if(now - start > 29000) {//wait 25 seconds before returning.
				//obtain new ticket
				CricketHelper.getDB().close();
				return CricketHelper.parseJsonReturn("no_data_changed",  ticket, "result");
			}
			if(this.newDataFound(user.getUser().getId())) {
				UnitControl uc = new UnitControl();
				Set<Unit> units = uc.getUnits();

				CricketHelper.getDB().close();
				return CricketHelper.parseJsonReturn(units,  ticket, "unit");
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				//so what.
			}
		}
		
	}
	private boolean newDataFound(String userid) {
		String qry = "SELECT 'yes' FROM attribute_history where event_time > (select max(issuetime) from tickets where userid ="+userid+")";
		try {
			CricketHelper.getDB().findOne(qry);
		} catch (NoDataException e) {
			return false;
		}
		return true;
	}
}