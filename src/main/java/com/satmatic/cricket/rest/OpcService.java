package com.satmatic.cricket.rest;

import java.io.FileNotFoundException;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import com.satmatic.cricket.mvc.control.AttributeControl;
import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.mvc.model.Attribute;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;
import com.satmatic.cricket.server.CricketSettings;
import com.satmatic.cricket.server.DBConnector;
import com.satmatic.cricket.server.opc.OpcConnector;

@Path("opc")
public class OpcService {
	
	final static Logger logger = Logger.getLogger(OpcService.class);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/save")
	public String save(@FormParam(value = "auth_ticket") String ticket, @FormParam(value = "unit") String unit, 
			@FormParam(value = "attribute") String attribute, @FormParam(value = "attr_value") String value) {		
		UserControl user = new UserControl(CricketHelper.getUserByTicket(ticket));
		//get the unit
		UnitControl uc = new UnitControl();
		Unit u = uc.findByCode(unit);
		
		/***This should work... it is a hack tho***/
		if(attribute.startsWith("light_set")) {
			String set = attribute.replaceAll("light_set_", "");
			
			String attr = "Kasvatusvalaistus"+set+".ValotPäälle";
			String part = CricketHelper.parseUPCPath(u.getOpcname()+"."+attr); 
			String opcPath = "ns=3;s="+part;
			Object saveValue = value;
			saveValue = Boolean.parseBoolean(value);
			OpcConnector opc = new OpcConnector();
			try {
				opc.connect(CricketSettings.getInstance());
				opc.setNodeValue(NodeId.parse(opcPath), saveValue);
				opc.release(CricketSettings.getInstance());
			} 
			catch (FileNotFoundException | InterruptedException | ExecutionException e) {
				logger.error("Error when connecting to logic", e);
				e.printStackTrace();
			}
			attr = "Kasvatusvalaistus"+set+".ValotPois";
			part = CricketHelper.parseUPCPath(u.getOpcname()+"."+attr); 
			opcPath = "ns=3;s="+part;
			saveValue = value;
			saveValue = !Boolean.parseBoolean(value);
			try {
				opc.connect(CricketSettings.getInstance());
				opc.setNodeValue(NodeId.parse(opcPath), saveValue);
				opc.release(CricketSettings.getInstance());
			} 
			catch (FileNotFoundException | InterruptedException | ExecutionException e) {
				logger.error("Error when connecting to logic", e);
				e.printStackTrace();
			}
		}
		/********************************************************************/
		else {
			//get the attribute
			AttributeControl ac = new AttributeControl(u);
			Attribute a = ac.findByCode(attribute);
	
			ac.setAttrib(a);
			
			String part = CricketHelper.parseUPCPath(u.getOpcname()+"."+a.getOpcname()); 
			String opcPath = "ns=3;s="+part;
			Object saveValue = value;
			switch(a.getType()) {
				case "float":
					saveValue = Float.parseFloat(value);
					break;
				case "boolean":
					saveValue = Boolean.parseBoolean(value);
					break;
				
			}
			//save to logic
			OpcConnector opc = new OpcConnector();
			try {
				opc.connect(CricketSettings.getInstance());
				opc.setNodeValue(NodeId.parse(opcPath), saveValue);
				opc.release(CricketSettings.getInstance());
			} 
			catch (FileNotFoundException | InterruptedException | ExecutionException e) {
				logger.error("Error when connecting to logic", e);
				e.printStackTrace();
			}
			//save to history
			ac.archive();
			
			//save to attribute unit
			a.setValue(value);
			ac.setAttrib(a);
			ac.save();
			
			CricketHelper.getDB().close();
		}
		return CricketHelper.parseJsonReturn("success", CricketHelper.obtainTicket(user.getUser()), "opc");
	}
}
