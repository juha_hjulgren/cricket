 package com.satmatic.cricket.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.mvc.model.User;
import com.satmatic.cricket.server.CricketHelper;

@Path("user")
public class UserService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(@QueryParam(value = "username") String uname, @QueryParam(value = "auth_ticket") String ticket) {
		
		//get user from ticket if ticket is valid. This needs to go to helpers.
		UserControl currUser = new UserControl(CricketHelper.getUserByTicket(ticket));
		
		UserControl uc = new UserControl();
		
		//get the User object for serialization
		User u;
		String ttr = CricketHelper.obtainTicket(currUser.getUser());
		try {
			u = uc.findByUserName(uname);
		} catch (NoDataException e) {
			return CricketHelper.parseJsonReturn("no_such_user", ttr, "user"); 
		}
		CricketHelper.getDB().close();
		//return the desired user with a new ticket.
		return CricketHelper.parseJsonReturn(u, ttr, "user");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("list")
	public String getUsers(@QueryParam(value = "auth_ticket") String ticket) {
		
		//get user from ticket if ticket is valid. This needs to go to helpers.
		UserControl currUser = new UserControl(CricketHelper.getUserByTicket(ticket));
		
		UserControl uc = new UserControl();
		
		//get the User object for serialization
		Set<User> users;
		String ttr = CricketHelper.obtainTicket(currUser.getUser());
		users= uc.listAll();
		CricketHelper.getDB().close();
		//return all users with a new ticket.
		return CricketHelper.parseJsonReturn(users, ttr, "user");
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String save(
			@FormParam(value = "username") String uname, 
			@FormParam(value = "realname") String rname,
			@FormParam(value = "email") String email,
			@FormParam(value = "old_pass") String pass,
			@FormParam(value = "new_pass") String newPass, 
			@FormParam(value = "user_group") String group,
			@FormParam(value = "auth_ticket") String ticket){
			
		//get user from ticket if ticket is valid. This needs to go to helpers.
		UserControl currUser = new UserControl(CricketHelper.getUserByTicket(ticket));
		
		UserControl uc = new UserControl();
		String id = "0";
		Map<String, String> vals = new HashMap<String, String>();
		try {
			User u = uc.findByUserName(uname);
			id = u.getId();
			vals.put("id", id);
			
		} catch (NoDataException e) {
			//no need to do anything here.
		}
		
		vals.put("username", uname);
		vals.put("realname", rname);
		vals.put("email", email);
		vals.put("group", group);
		vals.put("password", CricketHelper.generateHash(newPass));

		uc = new UserControl(vals);
		uc.save();
		String ttr = CricketHelper.obtainTicket(currUser.getUser());
		CricketHelper.getDB().close();
		return CricketHelper.parseJsonReturn(uc.getUser(), ttr, "user");
	}
}
