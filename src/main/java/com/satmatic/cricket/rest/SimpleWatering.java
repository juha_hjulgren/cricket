package com.satmatic.cricket.rest;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.satmatic.cricket.server.CricketHelper;
import com.satmatic.cricket.server.CricketSettings;
import com.satmatic.cricket.server.opc.OpcConnector;

@Path("water")
public class SimpleWatering {
	private final static Logger log = LoggerFactory.getLogger(SimpleWatering.class);
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/save")
	public String save(@FormParam(value = "auth_ticket") String ticket, 
			@FormParam(value = "unit") String unit, 
			@FormParam(value = "group") String group, 
			@FormParam(value = "open_value") String oValue, 
			@FormParam(value = "close_value") String cValue) {	
		
		//send to OPC-UA
		
		OpcConnector opc = new OpcConnector();
		try {
			opc.connect(CricketSettings.getInstance());			
			String opcName = "ns=3;s="+CricketHelper.parseUPCPath(unit+"."+group+".AukiAika");
			System.out.println("Set open time:"+opc.setNodeValue(NodeId.parse(opcName), Float.parseFloat(oValue)));
			opcName = "ns=3;s="+CricketHelper.parseUPCPath(unit+"."+group+".Taukoaika");
			System.out.println("Set close time:"+opc.setNodeValue(NodeId.parse(opcName), Integer.parseInt(cValue)));
		} catch (FileNotFoundException | InterruptedException | ExecutionException e) {
			//log exception
			e.printStackTrace();
			return CricketHelper.parseJsonReturn("FAIL", ticket, "watering_set");
		}
		return CricketHelper.parseJsonReturn("SUCCESS", ticket, "watering_set");
	}
	
	//TODO
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/stop")
	public String stop(@FormParam(value = "auth_ticket") String ticket, 
	@FormParam(value = "unit") String unit, 
	@FormParam(value = "group") String group){
		
		return "success";
	}
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/read")
	public String getValues(@QueryParam(value = "auth_ticket") String ticket, 
			@QueryParam(value = "unit") String unit, 
			@QueryParam(value = "group") String group) {
		
		Map<String, String> toRet = new HashMap<String, String>();
		
		OpcConnector opc = new OpcConnector();
		try {
			opc.connect(CricketSettings.getInstance());
			//opc.debugNodes("", Identifiers.RootFolder);
			//once i know the structure this will be good to go
			String opcName = "ns=3;s="+CricketHelper.parseUPCPath(unit+"."+group+".AukiAika");
			String oValue = opc.getNodeValue(NodeId.parse(opcName));
			opcName = "ns=3;s="+CricketHelper.parseUPCPath(unit+"."+group+".Taukoaika");
			
			String cValue = opc.getNodeValue(NodeId.parse(opcName));
			toRet.put("open_value", oValue);
			toRet.put("close_value", cValue);
			return CricketHelper.parseJsonReturn(toRet, ticket, "watering");
		} catch (FileNotFoundException | InterruptedException | ExecutionException e) {
			//log exception
			log.error("Failure getting value from OPC UA", e);
			e.printStackTrace();
			return CricketHelper.parseJsonReturn("FAIL", ticket, "watering_set");
		}
		
	}
}
