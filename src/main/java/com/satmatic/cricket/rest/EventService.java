package com.satmatic.cricket.rest;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.satmatic.cricket.mvc.control.EventControl;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.mvc.model.Event;
import com.satmatic.cricket.server.CricketHelper;


@Path("event")
public class EventService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/list")
	public String listEvents(@QueryParam(value = "auth_ticket") String ticket) {
		UserControl uc = new UserControl(CricketHelper.getUserByTicket(ticket));
		ticket = CricketHelper.checkTicket(uc.getUser(), ticket);
		EventControl ec = new EventControl();
		Set<Event> toRet = ec.findAll();
		CricketHelper.getDB().close();
		return CricketHelper.parseJsonReturn(toRet, ticket, "event");	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/unitlist")
	public String listEvents(@QueryParam(value = "auth_ticket") String ticket, @QueryParam(value = "unit_id") String uId) {
		UserControl uc = new UserControl(CricketHelper.getUserByTicket(ticket));
		ticket = CricketHelper.checkTicket(uc.getUser(), ticket);
		EventControl ec = new EventControl();
		Set<Event> toRet = ec.findByUnit(Integer.parseInt(uId));
		CricketHelper.getDB().close();
		return CricketHelper.parseJsonReturn(toRet, ticket, "event");	
	}
}
