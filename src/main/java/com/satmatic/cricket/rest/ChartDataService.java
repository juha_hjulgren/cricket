package com.satmatic.cricket.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.satmatic.cricket.mvc.control.AttributeControl;
import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.model.Attribute;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;

/**
 * This class does not really conform to MVC principles, due to the nature of the data.
 * TODO: there might be some areas where we could simplify things. Some might not be needed too.
 * 
 */
@Path("chart")
public class ChartDataService {

	/**
	 * Get a day worth of chart data. 
	 * @param unitId to get the data for
	 * @param ticket to recognize the user
	 * @return weekly data parsed to a format that browser chart.js understands.
	 * @throws ParseException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/day")
	public String getDayData(@QueryParam(value = "unit_id") int unitId, @QueryParam(value = "auth_ticket") String ticket) throws ParseException {
		String toRet = doPreParse(unitId, "INTERVAL  1 DAY", "dd.MM.yyyy hh:00", "dd.MM hh:00", ticket);
		CricketHelper.getDB().close();
		return toRet;
	}
	
	/**
	 * Get a 3 day worth of chart data. 
	 * @param unitId to get the data for
	 * @param ticket to recognize the user
	 * @return weekly data parsed to a format that browser chart.js understands.
	 * @throws ParseException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/3day")
	public String get3DayData(@QueryParam(value = "unit_id") int unitId, @QueryParam(value = "auth_ticket") String ticket) throws ParseException {
		String toRet = doPreParse(unitId, "INTERVAL  3 DAY", "dd.MM.yyyy hh:00", "dd.MM hh:00", ticket); 
		CricketHelper.getDB().close();
		return toRet;
	}
	
	/**
	 * Get a month worth of chart data. 
	 * @param unitId to get the data for
	 * @param ticket to recognize the user
	 * @return weekly data parsed to a format that browser chart.js understands.
	 * @throws ParseException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/month")
	public String getMonthData(@QueryParam(value = "unit_id") int unitId, @QueryParam(value = "auth_ticket") String ticket) throws ParseException {
		String toRet = doPreParse(unitId, "INTERVAL  30 DAY", "dd.MM.yyyy", "dd.MM", ticket); 
		CricketHelper.getDB().close();
		return toRet;
	}
	
	/**
	 * Get a week worth of chart data. 
	 * @param unitId to get the data for
	 * @param ticket to recognize the user
	 * @return weekly data parsed to a format that browser chart.js understands.
	 * @throws ParseException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/week")
	public String getWeekData(	@QueryParam(value = "unit_id") int unitId,  @QueryParam(value = "auth_ticket") String ticket) throws ParseException {
		String toRet = doPreParse(unitId, "INTERVAL  7 DAY", "dd.MM.yyyy", "dd.MM", ticket);
		CricketHelper.getDB().close();
		return toRet;
	}
	
	private String doPreParse(int unitId, String interval, String keyFormat, String viewFormat, String ticket) throws ParseException {
		
		//delete anything older than 30 days here? Or just move it to an archive?
		String del = "DELETE FROM attribute_history WHERE event_time < (NOW()-INTERVAL 31 DAY)";
		CricketHelper.getDB().execute(del);
		
		UnitControl uc = new UnitControl();
		Unit u = uc.findById(unitId);
		
		List<Map<?, ?>> datasets = new ArrayList<Map<?, ?>>();
		
		//TODO Configurable?
		String[] attr = {"in_temp", "pre_temp", "on_temp", "humidity", "co2"};
		String[] color = {"#3e95cd", "#754322", "#c45850", "#8e5ea2", "#3cba9f"};
		
		Map<String, List<?>> toParse = null;
		Set<String> labels = new HashSet<String>();
		
		for(int i = 0;i < attr.length;++i) {
			AttributeControl ac = new AttributeControl(u);
			Attribute a = ac.findByCode(attr[i]);
			toParse = this.getData(unitId, a.getId(), interval, keyFormat);
			
			Map<String, Object> dataSet = new HashMap<String, Object>();
			for(Object ss : toParse.get("labels")) {
				labels.add(ss.toString());
			}
			dataSet.put("data", this.parseData(toParse, keyFormat, viewFormat));
			dataSet.put("fill", false);
			dataSet.put("borderColor", color[i]);
			dataSet.put("label", a.getName());
			dataSet.put("lineTension", 0);
			datasets.add(dataSet);
		}
		
		List<String> lret = new ArrayList<String>(labels);
		DateFormat df = new SimpleDateFormat(keyFormat);
		// Sort the dates....
		Collections.sort(lret, new Comparator<String>() {
			public int compare(String o1, String o2) {
				//these Strings are actually dates.
				try {
					return df.parse(o1).compareTo(df.parse(o2));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return 0;
				}
			 }
		});
		List<String> fLabels = new ArrayList<String>();

		DateFormat ldf = new SimpleDateFormat(viewFormat);
		for(String ss : lret) {
			fLabels.add(ldf.format(df.parse(ss)));
		}
		Map toRet = new HashMap();
		toRet.put("labels", fLabels);
		toRet.put("datasets", datasets);
		
		return CricketHelper.parseJsonReturn(toRet, ticket, "chart");
	}
	
	private Object parseData(Map<String, List<?>> toParse, String keyFormat, String viewFormat) throws ParseException {
		DateFormat ldf = new SimpleDateFormat(viewFormat);
		DateFormat df = new SimpleDateFormat(keyFormat);

		Set<Map<String, String>> toRet = new LinkedHashSet<Map<String, String>>();
		List x = toParse.get("labels");
		List y = toParse.get("data");
		int count = 0;
		for(Object oo : x) {
			Map<String, String> drow = new HashMap<String, String>();
			drow.put("x", ldf.format(df.parse(oo.toString())));
			drow.put("y", y.get(count).toString());
			toRet.add(drow);
			++count;			
		}
		return toRet;
	}

	/** Holy hell, this made one big method. 
	 * 
	 * @param unitId id of the unit
	 * @param attribId id of the attribute
	 * @return the values of a certain attribute on a given time in a map containing lists.
	 * @throws ParseException if dates get broken in db which should not ever happen.
	 */
	private Map<String, List<?>> getData(int unitId, int attribId, String interval, String keyFormat) throws ParseException {
	
		String query = "select value, event_time from attribute_history "
				+ "where event_time >=(NOW() - "+interval+") "
				+ " and unit_id = "+ unitId
				+ " and attrib_id = "+ attribId
				+ " AND value != 'null'";
		
		Map<String, Double> days = new HashMap<String, Double>();
		Map<String, Integer> counts = new HashMap<String, Integer>();
		DateFormat df = new SimpleDateFormat(keyFormat);
		//get data from db.
		Set<Map<String, String>> vals = CricketHelper.getDB().findAll(query);
		for(Map<String, String> mm : vals) {
			String day = mm.get("event_time");
			
			//we want to put date as 01.01.2018 etc.
			
			//format from DB TODO:to cricketHelper!
			DateFormat fromDb = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
			Date d = fromDb.parse(day);
			day = df.format(d);
			
			//get the value as real, so use double
			if(mm.get("value").equals("null"))continue;
			double value = Double.parseDouble(mm.get("value"));
			
			if(days.containsKey(day)) {
				counts.put(day,  counts.get(day) + 1);
				double old = days.get(day);
				old += value;
				old /= counts.get(day);
				days.put(day, old);	
			}
			else {
				days.put(day, value);
				counts.put(day, 1);
			}
		}
		//put the dates to a list here.
		List<String> labels = new ArrayList<String>(days.keySet());
		
		// Sort the dates....
		Collections.sort(labels, new Comparator<String>() {
			private DateFormat df = new SimpleDateFormat(keyFormat);
			
			public int compare(String o1, String o2) {
				//these Strings are actually dates.
				try {
					return df.parse(o1).compareTo(df.parse(o2));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return 0;
				}
			 }
		});
		//use the sorted key list to sort these.
		List<String> data = new ArrayList<String>();

		for(String ss : labels) {
			data.add(days.get(ss).toString());
		}
		Map<String, List<?>> toRet = new HashMap<String, List<?>>();
		
		toRet.put("labels", labels);
		toRet.put("data", data);
		
		return toRet;
	}
	
	
}
