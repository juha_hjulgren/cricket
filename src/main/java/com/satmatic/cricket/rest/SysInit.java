package com.satmatic.cricket.rest;

import java.io.FileNotFoundException;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.satmatic.cricket.mvc.control.UnitControl;
import com.satmatic.cricket.mvc.model.Unit;
import com.satmatic.cricket.server.CricketHelper;

@Path("sys")
public class SysInit {
	
	@GET
	@Path("create")
	public void createUnits() {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			String fname = classLoader.getResource("attrib_descr.json").getFile().toString();
			Set<Unit> units = CricketHelper.readFromFile(fname);
			UnitControl uc = null; 
			for(Unit uu :units) {
				uc = new UnitControl(uu);
				uc.save();
			}
			CricketHelper.getDB().close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
