package com.satmatic.cricket.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXB;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.satmatic.cricket.mvc.control.DayControl;
import com.satmatic.cricket.mvc.control.HourControl;
import com.satmatic.cricket.mvc.control.PeriodControl;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.reader.xml.Day;
import com.satmatic.cricket.reader.xml.Days;
import com.satmatic.cricket.reader.xml.Hour;
import com.satmatic.cricket.reader.xml.Hours;
import com.satmatic.cricket.reader.xml.Periods;
import com.satmatic.cricket.server.CricketHelper;

@Path("file")
public class FileService {

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String uploadFile(/*@FormDataParam("file") InputStream fInput,
			@FormDataParam("file") FormDataContentDisposition fDetail, */
			@FormParam("type") String fType,
			@FormParam(value = "auth_ticket") String ticket){
		String toRet = "";//readFile(fInput, fDetail, fType, ticket);
		return toRet;
	}

	private String readFile(InputStream fInput, FormDataContentDisposition fDetail, String fType, String ticket){
		
		//TODO, make a function out of this.
		UserControl uc = new UserControl(CricketHelper.getUserByTicket(ticket));
		//get ticket
		String toRet = CricketHelper.obtainTicket(uc.getUser());
		
		CricketHelper.getDB().close();
		String xml = "";
		//check which file type
		//create correct objects according to file type
		switch(fType) {
			case "hour":
				HourControl hc = new HourControl();
				Hours hours = JAXB.unmarshal(fInput, Hours.class);
				
				//now we have the uploaded data, need to merge with what we have in DB.
				//We can do that by saving.Since we are in a hurry, no questions asked.
				for(Hour h : hours.getHours()) {
					hc.setHour(h);
					hc.save();
				}
				// get everything.
				Set<Hour> hSet = hc.listAll();
				hours.setHours(new ArrayList<Hour>(hSet));
				JAXB.marshal(hours, xml);
			case "day":
				DayControl dc = new DayControl();
				Days days = JAXB.unmarshal(fInput, Days.class);
				
				for(Day d : days.getDays()) {
					dc.setDay(d);
					dc.save();
				}
				Set<Day> dSet = dc.listAll();
				days.setDays(new ArrayList<Day>(dSet));
				JAXB.marshal(days, xml);
			case "period":
				//then for the periods...Not much different yet from the others.
				PeriodControl pc = new PeriodControl();
				Periods periods = JAXB.unmarshal(fInput, Periods.class);
				return "data_goes_here";
		}
	
		//return the data as string
		return CricketHelper.parseJsonReturn(xml, toRet, "payload");
	}
}
