package com.satmatic.cricket.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.satmatic.cricket.NoDataException;
import com.satmatic.cricket.mvc.control.UserControl;
import com.satmatic.cricket.mvc.model.User;
import com.satmatic.cricket.server.CricketHelper;

@Path("login")
public class LoginService {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String testConnectivity(@QueryParam(value = "auth_ticket") String ticket) {
		//here we need to check uname exists, 
		UserControl uc = new UserControl(CricketHelper.getUserByTicket(ticket));
		//get ticket
		String toRet = CricketHelper.obtainTicket(uc.getUser());
		
		CricketHelper.getDB().close();
		
		return CricketHelper.parseJsonReturn(uc.getUser(), toRet, "user");

	}
	/**
	 * This method is used for user login to the system.
	 * @param uname username String
	 * @param pword password in plain.
	 * @return User object, plus the ticket to be used for furthermore calls.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public String tryLogin(@FormParam(value = "login-username") String uname, @FormParam(value = "login-password") String pword) {
		

		if(uname.equals("") || pword.equals("")) {
			return "user_login_fail";
		}
		//here we need to check uname exists, 
		UserControl uc = new UserControl();
		//get the User object for serialization
		User u;
		try {
			u = uc.findByUserName(uname);
		} catch (NoDataException e) {
			return "user_login_fail";
		}
		
		//hash and salt password
		String hashpw = CricketHelper.generateHash(pword);
		//check password matches
		if(!u.getPassWord().equals(hashpw)) {
			return "user_login_fail";
		}
		//get ticket
		String ticket = CricketHelper.obtainTicket(u);
		
		CricketHelper.getDB().close();
		
		return CricketHelper.parseJsonReturn(u, ticket, "user");
	}
}