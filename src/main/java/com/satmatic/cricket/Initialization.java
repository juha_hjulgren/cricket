package com.satmatic.cricket;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.satmatic.cricket.server.CricketHelper;
import com.satmatic.cricket.server.CricketSettings;
import com.satmatic.cricket.server.DBConnector;

public class Initialization {
	
	public void initialize() throws SQLException {
		//init db
		this.initDB();
		//init admin user
		this.initAdmin();
		//init application settings
		this.initAppSettings();
		//init units and parameters
		this.initUnits();
	}

	private void initUnits() {
		// TODO Auto-generated method stub
		
	}

	private void initAppSettings() {
		// TODO Auto-generated method stub
		
	}

	private void initAdmin() {
		DBConnector dbconn = new DBConnector();
		
		dbconn.connect(CricketSettings.getInstance());
		
		//create administrative user
		String sql = "INSERT INTO TABLE user(username, realname, email, password, group)"+
				"VALUES('admin_cricket', 'Administrative Default User', '"+
				CricketSettings.getInstance().getParam(CricketHelper.ADMIN_EMAIL)+"', '"
				+CricketHelper.generateHash(CricketHelper.DEFAULT_PASS)+"', 'admin')";
		dbconn.execute(sql);
	}

	private void initDB() throws SQLException{
		//connect to mysql.
		String url = "jdbc:mysql://localhost";
		// Defines username and password to connect to database server.
		String username = "root";
		String password = "root";
		// first, create the database
		Connection conn = DriverManager.getConnection(url, username, password);
		String sql = "CREATE DATABASE IF NOT EXISTS cricket";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.execute();
		
		//we should be able to change from raw mode to our own class...
		DBConnector dbconn = CricketHelper.getDB();
		
		//create user table
		sql = "CREATE TABLE user(" + 
				"id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, " + 
				"username VARCHAR(30) NOT NULL, " + 
				"realname VARCHAR(100) NOT NULL, " + 
				"email VARCHAR(50), " + 
				"password CHAR(40) NOT NULL, " + 
				"usergroup ENUM('viewer', 'user', 'admin') " + 
				");";
		
		dbconn.execute(sql);
		
		sql = "INSERT INTO user(username, realname, email, password, usergroup)" + 
				"VALUES('admin_cricket', 'Administrative Default User', 'admin@localhost', '9613d18532e172fdc69f2cec8fdecc56e9a7fad2', 'admin')";
		
		dbconn.execute(sql);
		
		//create Unit table
		sql = "CREATE TABLE unit(" + 
				"id INT(3) UNSIGNED AUTO INCREMENT PRIMARY KEY, " + 
				"name VARCHAR(50) NOT NULL, " + 
				"code VARCHAR(30) NOT NULL, " + 
				"opcname VARCHAR(30) NOT NULL " + 
				")";
		dbconn.execute(sql);
		
		//Create ticket storage
		sql = "CREATE TABLE tickets( "+
				"userid INT(6)UNSIGNED, " + 
				"ticket CHAR(16) NOT NULL, " + 
				"issuetime TIMESTAMP" +
				")";
		dbconn.execute(sql);
		
		//Create settings table
		sql = "CREATE TABLE settings( "+
				"id INT(6)UNSIGNED, " + 
				"key VARCHAR(255) NOT NULL, " + 
				"value VARCHAR(255) NOT NULL " +
				")";
		dbconn.execute(sql);
		//Create Attribute table
		sql = "CREATE TABLE attribute( " + 
				"id INT(6) UNSIGNED AUTO INCREMENT PRIMARY KEY," + 
				"name VARCHAR(50) NOT NULL, " + 
				"code VARCHAR(30) NOT NULL, " + 
				"opcname VARCHAR(30) NOT NULL, " + 
				"value VARCHAR(255) NOT NULL, " + 
				"type ENUM('string', 'boolean', 'float') NOT NULL, " + 
				"editable CHAR(0) " + 
				")";
		dbconn.execute(sql);
		
		//Create unit_property table
		sql = "CREATE TABLE unit_attribute(" + 
				"unit_id INT(3) NOT NULL, " + 
				"attrib_id INT(6) NOT NULL" + 
				")";
		dbconn.execute(sql);
		//Create history table
		
		//Create logging table
		
		//Create alarm table
		
    }	
}
